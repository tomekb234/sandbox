package sandbox.engine;

import java.nio.ByteBuffer;

// native graphics interface
public class GraphicEngine {
    private static native void setup();
    static { setup(); }

    public static final int keyForward = 0;
    public static final int keyBackward = 1;
    public static final int keyLeft = 2;
    public static final int keyRight = 3;
    public static final int keyUp = 4;
    public static final int keyDown = 5;

    // used by JNI
    @SuppressWarnings("unused")
    private long engine;
    @SuppressWarnings("unused")
    private long buffer;

    private native void init();

    public GraphicEngine() {
        init();
    }

    public native void start();

    public native void stop();

    public native ByteBuffer newBuffer(int width, int height, boolean ownWindow);

    public native boolean nextFrameAvailable();

    public native void nextFrameAcknowledge();

    public native void registerMesh(Mesh mesh);

    public native void addEntity(NativeEntity entity);

    public native void removeEntity(NativeEntity entity);

    public native void addLight(NativeLight light);

    public native void removeLight(NativeLight light);

    public native void setBackgroundColor(Vector3f color);

    public native void setBrightness(float brightness);

    public native void setGammaCorrection(float gamma);

    public native void mouseMoved(int dx, int dy);

    public native void keyPressed(int key);

    public native void keyReleased(int key);

    public native Vector3f getCameraPosition();

    public native Vector3f getCameraDirection();

    public native Vector3f mouseDirection(int x, int y);

    public native void setFpsLimit(int limit);

    public native int fps();
}
