package sandbox.engine;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

// utility functions for simple shapes generation
public class Models {
    public static class ModelData {
        public final int size;
        public final float[] vertices, normals;

        public ModelData(float[] v, float[] n) {
            vertices = v;
            normals = n;
            size = v.length / 3;
        }
    }

    private static class ModelBuilder {
        private ArrayList<Float> points = new ArrayList<>();
        private ArrayList<Float> normals = new ArrayList<>();

        private ArrayList<Integer> pointIndices = new ArrayList<>();
        private ArrayList<Integer> normalIndices = new ArrayList<>();

        private float comX = 0.0f, comY = 0.0f, comZ = 0.0f;

        public void point(float x, float y, float z) {
            points.add(x);
            points.add(y);
            points.add(z);
        }

        public void normal(float x, float y, float z) {
            normals.add(x);
            normals.add(y);
            normals.add(z);
        }

        public void vertexPos(int i) {
            pointIndices.add(i);
        }

        public void vertexNorm(int i) {
            normalIndices.add(i);
        }

        public void triangle(int p1, int p2, int p3, int n1, int n2, int n3) {
            vertexPos(p1);
            vertexPos(p2);
            vertexPos(p3);
            vertexNorm(n1);
            vertexNorm(n2);
            vertexNorm(n3);
        }

        public void centerOfMass(float x, float y, float z) {
            comX = x;
            comY = y;
            comZ = z;
        }

        private float[] indicesToArray(ArrayList<Float> values, ArrayList<Integer> indices) {
            var resultTab = new float[indices.size() * 3];
            for (int i = 0, j = 0; i < indices.size(); i++)
                for (int k = 0; k < 3; k++, j++)
                    resultTab[j] = values.get(3 * indices.get(i) + k);
            return resultTab;
        }

        public float[] getVertexTab() {
            var vTab = indicesToArray(points, pointIndices);
            var com = new float[] { comX, comY, comZ };
            for (var i = 0; i < vTab.length; i++)
                vTab[i] -= com[i % 3];
            return vTab;
        }

        public float[] getNormalTab() {
            return indicesToArray(normals, normalIndices);
        }

        public ModelData getData() {
            return new ModelData(getVertexTab(), getNormalTab());
        }
    }

    public static ModelData buildCuboid(float halfX, float halfY, float halfZ) {
        var builder = new ModelBuilder();

        for (var i = 0; i < 8; i++)
            builder.point(halfX * (i % 2 * 2 - 1),
                halfY * (i / 4 * 2 - 1),
                halfZ * (i / 2 % 2 * 2 - 1));
        for (var i = 0; i < 6; i++)
            builder.normal(i == 2 ? -1.0f : (i == 3 ? 1.0f : 0.0f),
                i == 4 ? 1.0f : (i == 5 ? -1.0f : 0.0f),
                i == 0 ? 1.0f : (i == 1 ? -1.0f : 0.0f));

        builder.triangle(2, 3, 7, 0, 0, 0);
        builder.triangle(2, 7, 6, 0, 0, 0); // front
        builder.triangle(1, 0, 4, 1, 1, 1);
        builder.triangle(1, 4, 5, 1, 1, 1); // back
        builder.triangle(0, 2, 6, 2, 2, 2);
        builder.triangle(0, 6, 4, 2, 2, 2); // left
        builder.triangle(3, 1, 5, 3, 3, 3);
        builder.triangle(3, 5, 7, 3, 3, 3); // right
        builder.triangle(6, 7, 5, 4, 4, 4);
        builder.triangle(6, 5, 4, 4, 4, 4); // top
        builder.triangle(0, 1, 3, 5, 5, 5);
        builder.triangle(0, 3, 2, 5, 5, 5); // bottom

        return builder.getData();
    }

    public static ModelData buildSphere(float radius, int meridians, int parallels) {
        var builder = new ModelBuilder();

        builder.point(0.0f, -radius, 0.0f);
        builder.normal(0.0f, -1.0f, 0.0f);

        for (var p = 1; p <= parallels; p++) {
            var y = Math.sin(Math.PI / (1 + parallels) * p - Math.PI / 2) * radius;
            var circleRadius = Math.sqrt(radius * radius - y * y);
            for (var m = 0; m < meridians; m++) {
                var x = Math.cos(2 * Math.PI / meridians * m) * circleRadius;
                var z = Math.sin(2 * Math.PI / meridians * m) * circleRadius;
                builder.point((float)x, (float)y, (float)z);
                builder.normal((float)x, (float)y, (float)z);
            }
        }

        builder.point(0.0f, radius, 0.0f);
        builder.normal(0.0f, 1.0f, 0.0f);

        for (var m = 0; m < meridians; m++)
            builder.triangle(0, m + 1, (m + 1) % meridians + 1,
                0, m + 1, (m + 1) % meridians + 1);

        for (var p = 0; p < parallels - 1; p++) {
            for (var m = 0; m < meridians; m++) {
                var a = 1 + p * meridians + m;
                var b = 1 + p * meridians + (m + 1) % meridians;
                builder.triangle(a, a + meridians, b, a, a + meridians, b);
                builder.triangle(b, a + meridians, b + meridians, b, a + meridians, b + meridians);
            }
        }

        for (var i = 0; i < meridians; i++)
            builder.triangle(1 + (parallels - 1) * meridians + i,
                1 + parallels * meridians,
                1 + (parallels - 1) * meridians + (i + 1) % meridians,
                1 + (parallels - 1) * meridians + i,
                1 + parallels * meridians,
                1 + (parallels - 1) * meridians + (i + 1) % meridians);

        return builder.getData();
    }

    public static ModelData buildFrustumCone(float bigRadius, float smallRadius, float height, int meridians) {
        var builder = new ModelBuilder();
        builder.centerOfMass(0.0f, height / 2, 0.0f);

        builder.point(0.0f, 0.0f, 0.0f);
        builder.normal(0.0f, -1.0f, 0.0f);

        for (var m = 0; m < meridians; m++) {
            var angle = m * 2 * Math.PI / meridians;
            var x = Math.cos(angle);
            var z = Math.sin(angle);
            builder.point((float)x * bigRadius, 0.0f, (float)z * bigRadius);
            builder.point((float)x * smallRadius, height, (float)z * smallRadius);
            var beta = Math.atan((bigRadius - smallRadius) / height);
            var ny = Math.sin(beta);
            var xz = Math.cos(beta);
            builder.normal((float)(x * xz), (float)ny, (float)(z * xz));
        }

        builder.point(0.0f, height, 0.0f);
        builder.normal(0.0f, 1.0f, 0.0f);

        for (var m = 0; m < meridians; m++) {
            var a = 2 * m + 1;
            var b = 2 * ((m + 1) % meridians) + 1;
            var ma = m + 1;
            var mb = (m + 1) % meridians + 1;
            builder.triangle(0, a, b, 0, 0, 0);
            builder.triangle(b, a, a + 1, mb, ma, ma);
            builder.triangle(b, a + 1, b + 1, mb, ma, mb);
            builder.triangle(2 * meridians + 1, b + 1, a + 1,
                meridians + 1, meridians + 1, meridians + 1);
        }

        return builder.getData();
    }

    public static ModelData buildCylinder(float radius, float height, int meridians) {
        return buildFrustumCone(radius, radius, height, meridians);
    }

    public static ModelData buildCone(float radius, float height, int meridians) {
        return buildFrustumCone(radius, 0.0f, height, meridians);
    }

    public static float[] randomColors(float[] vertices, float r, float g, float b, float p) {
        var rand = new Random();
        var colors = new float[vertices.length];
        for (var i = 0; i < colors.length; i += 3) {
            var x = (rand.nextFloat() - 0.5f) * p;
            colors[i] = r + x;
            colors[i + 1] = g + x;
            colors[i + 2] = b + x;
        }
        return colors;
    }
}
