package sandbox.engine;

// native-side entity interface
public class NativeEntity {
    private static native void setup();
    static { setup(); }

    @SuppressWarnings("unused")
    private long entity; // used by JNI

    private native void init();

    public NativeEntity() {
        init();
    }

    public native void initMesh(Mesh mesh);

    public native void deinit();

    public native int getId();

    public native void setSize(float size);

    public native void setMass(float mass);

    public native void setImmovable(boolean immovable);

    public native void setFriction(float friction);

    public native void setBounciness(float bounciness);

    public native void setColor(Vector3f color);

    public native void setColorOpacity(float colorOpacity);

    public native void setAcceleration(Vector3f acceleration);

    public native Vector3f getPosition();

    public native void setPosition(Vector3f position);

    public native Vector3f getRotation();

    public native void setRotation(Vector3f rotation);

    public native Vector3f getVelocity();

    public native void setVelocity(Vector3f velocity);

    public native Vector3f getAngularVelocity();

    public native void setAngularVelocity(Vector3f angularVelocity);

    public native void setSelected(boolean selected);
}
