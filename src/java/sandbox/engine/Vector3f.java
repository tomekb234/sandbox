package sandbox.engine;

public class Vector3f {
    private static native void setup();
    static { setup(); }

    public static final Vector3f zero = new Vector3f(0, 0, 0);

    public final float x, y, z;

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "," + z + "]";
    }

    public static Vector3f valueOf(String s) {
        var t = s.substring(1, s.length() - 1);
        var a = t.split(",");
        return new Vector3f(Float.parseFloat(a[0]), Float.parseFloat(a[1]), Float.parseFloat(a[2]));
    }

    public Vector3f add(Vector3f vector) {
        return new Vector3f(x + vector.x, y + vector.y, z + vector.z);
    }

    public Vector3f multiply(float factor) {
        return new Vector3f(factor * x, factor * y, factor * z);
    }
}
