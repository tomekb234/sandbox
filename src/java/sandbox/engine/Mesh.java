package sandbox.engine;

public class Mesh {
    private static native void setup();
    static { setup(); }

    public static final int shapeTypeCuboid = 0;
    public static final int shapeTypeSphere = 1;
    public static final int shapeTypeCone = 2;
    public static final int shapeTypeCylinder = 3;
    public static final int shapeTypeConvexHull = 4;

    @SuppressWarnings("unused")
    private long mesh; // used by JNI

    private native void init(int shapeType, Vector3f shapeParams, int size);

    public Mesh(int shapeType, Vector3f shapeParams, int size) {
        init(shapeType, shapeParams, size);
    }

    private native void loadData(int dataType, float[] data);

    public void loadVertices(float[] data) {
        loadData(0, data);
    }

    public void loadColors(float[] data) {
        loadData(1, data);
    }

    public void loadNormals(float[] data){
        loadData(2, data);
    }

    public static Mesh cuboid(Vector3f halves) {
        var data = Models.buildCuboid(halves.x, halves.y, halves.z);
        var mesh = new Mesh(shapeTypeCuboid, halves, data.size);
        mesh.loadVertices(data.vertices);
        mesh.loadNormals(data.normals);
        mesh.loadColors(defaultColors(data.vertices));
        return mesh;
    }

    public static Mesh sphere(float radius) {
        var data = Models.buildSphere(radius, 30, 30);
        var mesh = new Mesh(shapeTypeSphere, new Vector3f(radius, radius, radius), data.size);
        mesh.loadVertices(data.vertices);
        mesh.loadNormals(data.normals);
        mesh.loadColors(defaultColors(data.vertices));
        return mesh;
    }

    public static Mesh cylinder(float radius, float height) {
        var data = Models.buildCylinder(radius, height, 30);
        var mesh = new Mesh(shapeTypeCylinder, new Vector3f(radius, height / 2, radius), data.size);
        mesh.loadVertices(data.vertices);
        mesh.loadNormals(data.normals);
        mesh.loadColors(defaultColors(data.vertices));
        return mesh;
    }

    public static Mesh cone(float radius, float height) {
        var data = Models.buildCone(radius, height, 30);
        var mesh = new Mesh(shapeTypeCone, new Vector3f(radius, height, radius), data.size);
        mesh.loadVertices(data.vertices);
        mesh.loadNormals(data.normals);
        mesh.loadColors(defaultColors(data.vertices));
        return mesh;
    }

    private static float[] defaultColors(float[] vertices) {
        return Models.randomColors(vertices, 0.5f, 0.5f, 0.5f, 0.5f);
    }
}
