package sandbox.engine;

// native-side light interface
public class NativeLight {
    public static native void setup();
    static { setup(); }

    public static final int lightTypePoint = 0;
    public static final int lightTypeDirectional = 1;

    // used by JNI
    @SuppressWarnings("unused")
    private long light;
    @SuppressWarnings("unused")
    private int type;

    private native void init(int type);

    public NativeLight(int type) {
        init(type);
    }

    public native void deinit();

    public native void setColor(Vector3f color);

    public native void setBrightness(float brightness);

    public native void setVector(Vector3f vector);
}
