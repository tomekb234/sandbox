package sandbox.engine;

// native physics interface
public class PhysicsEngine {
    private static native void setup();
    static { setup(); }

    @SuppressWarnings("unused")
    private long engine; // dla JNI

    private native void init();

    public PhysicsEngine() {
        init();
    }

    public native void start();

    public native void stop();

    public native void registerMesh(Mesh mesh);

    public native void addEntity(NativeEntity entity);

    public native void removeEntity(NativeEntity entity);

    public native void addRope(NativeRope rope);

    public native void removeRope(NativeRope rope);

    public native void setGravity(Vector3f gravity);

    public native void setPaused(boolean paused);

    public native void setSpeed(float speed);

    public native int rayCast(Vector3f pos, Vector3f dir);

    public native int entityCount();
}
