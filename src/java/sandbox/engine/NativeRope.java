package sandbox.engine;

public class NativeRope {
    private static native void setup();
    static { setup(); }

    @SuppressWarnings("unused")
    private long rope; // used by JNI

    private native void init(int entityIdA, int entityIdB);

    public NativeRope(int entityIdA, int entityIdB) {
        init(entityIdA, entityIdB);
    }

    public native void deinit();

    public native void setLength(float length);
}
