package sandbox.entity.light;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sandbox.engine.NativeLight;
import sandbox.engine.Vector3f;
import sandbox.utils.Utils;

public class PointLight extends Light {
    public final ObjectProperty<Vector3f> position;

    public PointLight() {
        super(NativeLight.lightTypePoint);
        position = new SimpleObjectProperty<>(Vector3f.zero);
        Utils.listen(position, nativeLight::setVector);
    }

    public PointLight(String name, Vector3f position) {
        this();
        this.name.set(name);
        this.position.set(position);
    }

    @Override
    public String type() {
        return "point-light";
    }
}
