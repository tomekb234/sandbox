package sandbox.entity.light;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;
import sandbox.Sandbox;
import sandbox.engine.NativeLight;
import sandbox.entity.Entity;
import sandbox.utils.BoundedFloatProperty;
import sandbox.utils.Utils;

// base class for lights
public abstract class Light extends Entity {
    private static final Color defaultColor = Color.WHITE;
    private static final float defaultBrightness = 10.0f;

    public final ObjectProperty<Color> color;
    public final FloatProperty brightness;
    protected NativeLight nativeLight;

    public Light(int type) {
        nativeLight = new NativeLight(type);
        color = new SimpleObjectProperty<>(defaultColor);
        brightness = new BoundedFloatProperty(defaultBrightness, true);
        Utils.listen(color, c -> nativeLight.setColor(Utils.color(c)));
        Utils.listen(brightness, nativeLight::setBrightness);
    }

    @Override
    public void attach(Sandbox sandbox) {
        sandbox.graphics.addLight(nativeLight);
    }

    @Override
    public void detach(Sandbox sandbox) {
        sandbox.graphics.removeLight(nativeLight);
    }

    @Override
    public void remove() {
        nativeLight.deinit();
    }
}
