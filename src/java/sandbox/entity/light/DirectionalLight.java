package sandbox.entity.light;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import sandbox.engine.NativeLight;
import sandbox.engine.Vector3f;
import sandbox.utils.Utils;

public class DirectionalLight extends Light {
    public final ObjectProperty<Vector3f> direction;

    public DirectionalLight() {
        super(NativeLight.lightTypeDirectional);
        direction = new SimpleObjectProperty<>(Vector3f.zero);
        Utils.listen(direction, nativeLight::setVector);
    }

    public DirectionalLight(String name, Vector3f direction) {
        this();
        this.name.set(name);
        this.direction.set(direction);
    }

    @Override
    public String type() {
        return "directional-light";
    }
}
