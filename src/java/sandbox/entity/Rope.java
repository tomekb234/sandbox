package sandbox.entity;

import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleBooleanProperty;
import sandbox.Sandbox;
import sandbox.engine.NativeRope;
import sandbox.engine.PhysicsEngine;
import sandbox.utils.BoundedFloatProperty;

import java.util.ArrayList;
import java.util.List;

public class Rope extends Group {
    private static final float defaultLength = 5.0f;
    private static final boolean defaultSequence = true;

    public final FloatProperty length;
    public final BooleanProperty sequence;
    protected List<NativeRope> nativeRopes;
    private PhysicsEngine physics;

    public Rope() {
        length = new BoundedFloatProperty(defaultLength, false);
        sequence = new SimpleBooleanProperty(defaultSequence);
        nativeRopes = new ArrayList<>();
        physics = null;
        children.addListener((Observable o) -> build());
        length.addListener(o -> update());
        sequence.addListener(o -> build());
    }

    public Rope(String name) {
        this();
        this.name.set(name);
    }

    @Override
    public String type() {
        return "rope";
    }

    @Override
    public void attach(Sandbox sandbox) {
        physics = sandbox.physics;
        for (var rope : nativeRopes)
            physics.addRope(rope);
    }

    @Override
    public void detach(Sandbox sandbox) {
        for (var rope : nativeRopes)
            physics.removeRope(rope);
        physics = null;
    }

    @Override
    public void remove() {
        for (var rope : nativeRopes)
            rope.deinit();
    }

    private void build() {
        for (var rope : nativeRopes) {
            if (physics != null)
                physics.removeRope(rope);
            rope.deinit();
        }

        nativeRopes.clear();

        var entities = new ArrayList<Integer>();
        for (var child : children) {
            if (child.id() != -1)
                entities.add(child.id());
        }

        for (var i = 1; i < entities.size(); i++) {
            var entityIdA = entities.get(sequence.get() ? i - 1 : 0);
            var entityIdB = entities.get(i);
            var rope = new NativeRope(entityIdA, entityIdB);
            rope.setLength(length.get());
            nativeRopes.add(rope);
            if (physics != null)
                physics.addRope(rope);
        }
    }

    private void update() {
        for (var rope : nativeRopes)
            rope.setLength(length.get());
    }
}
