package sandbox.entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

// group of multiple entities
public class Group extends Entity {
    protected ObservableList<Entity> children;

    public Group() {
        children = FXCollections.observableArrayList();
    }

    public Group(String name) {
        this();
        this.name.set(name);
    }

    @Override
    public String type() {
        return "group";
    }

    @Override
    public ObservableList<Entity> children() {
        return children;
    }

    @Override
    public boolean group() {
        return true;
    }
}
