package sandbox.entity;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sandbox.Sandbox;

// base class for all simulation entities
public abstract class Entity {
    public final StringProperty name;

    public Entity() {
        name = new SimpleStringProperty();
    }

    // entity type identifier
    public abstract String type();

    public ObservableList<Entity> children() {
        return FXCollections.emptyObservableList();
    }

    // only groups can have children
    public boolean group() {
        return false;
    }

    // used to identify native-side entities
    public int id() {
        return -1;
    }

    // adding to the simulation
    public void attach(Sandbox sandbox) { }

    public void detach(Sandbox sandbox) { }

    // called before removing last reference
    public void remove() { }

    public void select(boolean selected) { }
}
