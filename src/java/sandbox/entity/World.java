package sandbox.entity;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;
import sandbox.Sandbox;
import sandbox.utils.BoundedFloatProperty;
import sandbox.utils.Utils;
import sandbox.engine.Vector3f;

// the root entity
public class World extends Group {
    private static final Vector3f defaultGravity = new Vector3f(0f, -9.81f, 0f);
    private static final Color defaultBackgroundColor = Color.WHITE;
    private static final float defaultBrightness = 0.75f;
    private static final float defaultGammaCorrection = 2.2f;

    public final ObjectProperty<Vector3f> gravity;
    public final ObjectProperty<Color> backgroundColor;
    public final BoundedFloatProperty brightness;
    public final BoundedFloatProperty gammaCorrection;
    private InvalidationListener gravityListener;
    private InvalidationListener colorListener;
    private InvalidationListener brightnessListener;
    private InvalidationListener gammaListener;

    public World() {
        gravity = new SimpleObjectProperty<>(defaultGravity);
        backgroundColor = new SimpleObjectProperty<>(defaultBackgroundColor);
        brightness = new BoundedFloatProperty(defaultBrightness, true, 0, 1);
        gammaCorrection = new BoundedFloatProperty(defaultGammaCorrection, false, 1, 4);
    }

    public World(String name) {
        this();
        this.name.set(name);
    }

    @Override
    public String type() {
        return "world";
    }

    @Override
    public void attach(Sandbox sandbox) {
        gravityListener = Utils.listen(gravity, sandbox.physics::setGravity);
        colorListener = Utils.listen(backgroundColor, c -> sandbox.graphics.setBackgroundColor(Utils.color(c)));
        brightnessListener = Utils.listen(brightness, sandbox.graphics::setBrightness);
        gammaListener = Utils.listen(gammaCorrection, sandbox.graphics::setGammaCorrection);
    }

    @Override
    public void detach(Sandbox sandbox) {
        gravity.removeListener(gravityListener);
        backgroundColor.removeListener(colorListener);
        brightness.removeListener(brightnessListener);
        gammaCorrection.removeListener(gammaListener);
    }
}
