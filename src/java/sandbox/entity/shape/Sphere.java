package sandbox.entity.shape;

import sandbox.engine.Mesh;
import sandbox.engine.Vector3f;

public class Sphere extends Shape {
    public static final Mesh mesh = Mesh.sphere(1);

    public Sphere() {
        nativeEntity.initMesh(mesh);
    }

    public Sphere(String name, Vector3f position) {
        this();
        this.name.set(name);
        this.position.set(position);
    }

    @Override
    public String type() {
        return "sphere";
    }
}
