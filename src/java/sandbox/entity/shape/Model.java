package sandbox.entity.shape;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import sandbox.Sandbox;
import sandbox.engine.Vector3f;

// custom shape with loaded model
public class Model extends Shape {
    public final StringProperty model;

    public Model() {
        model = new SimpleStringProperty();
        colorOpacity.set(0);
    }

    public Model(String model, String name, Vector3f position) {
        this();
        this.model.set(model);
        this.name.set(name);
        this.position.set(position);
    }

    @Override
    public String type() {
        return "model";
    }

    @Override
    public void attach(Sandbox sandbox) {
        var mesh = sandbox.getModel(model.get());
        nativeEntity.initMesh(mesh != null ? mesh : Cube.mesh);
        super.attach(sandbox);
    }
}
