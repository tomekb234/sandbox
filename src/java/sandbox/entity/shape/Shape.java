package sandbox.entity.shape;

import javafx.beans.property.*;
import javafx.scene.paint.Color;
import sandbox.Sandbox;
import sandbox.entity.Entity;
import sandbox.utils.Utils;
import sandbox.engine.NativeEntity;
import sandbox.engine.Vector3f;
import sandbox.utils.BoundedFloatProperty;
import sandbox.utils.VolatileProperty;

// base class for physical entities
public abstract class Shape extends Entity {
    private static final float defaultSize = 1.0f;
    private static final float defaultMass = 1.0f;
    private static final float defaultFriction = 0.5f;
    private static final float defaultBounciness = 0.0f;
    private static final Color defaultColor = Color.color(0.2, 0.2, 0.2);
    private static final float defaultColorOpacity = 0.75f;
    private static final Vector3f defaultAcceleration = Vector3f.zero;

    public final FloatProperty size;
    public final FloatProperty mass;
    public final BooleanProperty immovable;
    public final BoundedFloatProperty friction;
    public final BoundedFloatProperty bounciness;
    public final ObjectProperty<Color> color;
    public final BoundedFloatProperty colorOpacity;
    public final ObjectProperty<Vector3f> acceleration;
    public final VolatileProperty<Vector3f> position;
    public final VolatileProperty<Vector3f> rotation;
    public final VolatileProperty<Vector3f> velocity;
    public final VolatileProperty<Vector3f> angularVelocity;
    protected NativeEntity nativeEntity;

    public Shape() {
        nativeEntity = new NativeEntity();

        size = new BoundedFloatProperty(defaultSize, false);
        mass = new BoundedFloatProperty(defaultMass, false);
        immovable = new SimpleBooleanProperty(false);
        friction = new BoundedFloatProperty(defaultFriction, true, 0, 1);
        bounciness = new BoundedFloatProperty(defaultBounciness, true, 0, 1);
        color = new SimpleObjectProperty<>(defaultColor);
        colorOpacity = new BoundedFloatProperty(defaultColorOpacity, true, 0, 1);
        acceleration = new SimpleObjectProperty<>(defaultAcceleration);
        position = new VolatileProperty<>(nativeEntity::getPosition, nativeEntity::setPosition);
        rotation = new VolatileProperty<>(nativeEntity::getRotation, nativeEntity::setRotation);
        velocity = new VolatileProperty<>(nativeEntity::getVelocity, nativeEntity::setVelocity);
        angularVelocity = new VolatileProperty<>(nativeEntity::getAngularVelocity, nativeEntity::setAngularVelocity);

        Utils.listen(size, nativeEntity::setSize);
        Utils.listen(mass, nativeEntity::setMass);
        Utils.listen(immovable, nativeEntity::setImmovable);
        Utils.listen(friction, nativeEntity::setFriction);
        Utils.listen(bounciness, nativeEntity::setBounciness);
        Utils.listen(color, c -> nativeEntity.setColor(Utils.color(c)));
        Utils.listen(colorOpacity, nativeEntity::setColorOpacity);
        Utils.listen(acceleration, nativeEntity::setAcceleration);
    }

    @Override
    public int id() {
        return nativeEntity.getId();
    }

    @Override
    public void attach(Sandbox sandbox) {
        // assuming that derived classes call nativeEntity.initMesh()
        sandbox.graphics.addEntity(nativeEntity);
        sandbox.physics.addEntity(nativeEntity);
    }

    @Override
    public void detach(Sandbox sandbox) {
        sandbox.graphics.removeEntity(nativeEntity);
        sandbox.physics.removeEntity(nativeEntity);
    }

    @Override
    public void remove() {
        nativeEntity.deinit();
    }

    @Override
    public void select(boolean selected) {
        nativeEntity.setSelected(selected);
    }
}
