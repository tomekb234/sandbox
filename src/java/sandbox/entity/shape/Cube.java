package sandbox.entity.shape;

import sandbox.engine.Mesh;
import sandbox.engine.Vector3f;

public class Cube extends Shape {
    public static final Mesh mesh = Mesh.cuboid(new Vector3f(1, 1, 1));

    public Cube() {
        nativeEntity.initMesh(mesh);
    }

    public Cube(String name, Vector3f position) {
        this();
        this.name.set(name);
        this.position.set(position);
    }

    @Override
    public String type() {
        return "cube";
    }
}
