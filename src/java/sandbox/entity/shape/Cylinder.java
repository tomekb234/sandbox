package sandbox.entity.shape;

import sandbox.engine.Mesh;
import sandbox.engine.Vector3f;

public class Cylinder extends Shape {
    public static final Mesh mesh = Mesh.cylinder(1, 2);

    public Cylinder() {
        nativeEntity.initMesh(mesh);
    }

    public Cylinder(String name, Vector3f position) {
        this();
        this.name.set(name);
        this.position.set(position);
    }

    @Override
    public String type() {
        return "cylinder";
    }
}
