package sandbox.entity.shape;

import sandbox.engine.Mesh;
import sandbox.engine.Vector3f;

public class Cone extends Shape {
    public static final Mesh mesh = Mesh.cone(1, 2);

    public Cone() {
        nativeEntity.initMesh(mesh);
    }

    public Cone(String name, Vector3f position) {
        this();
        this.name.set(name);
        this.position.set(position);
    }

    @Override
    public String type() {
        return "cone";
    }
}
