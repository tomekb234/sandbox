package sandbox.utils;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ObservableFloatValue;
import javafx.beans.value.ObservableIntegerValue;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import sandbox.engine.Vector3f;

import java.util.function.Consumer;
import java.util.function.Function;

// various utility functions
public class Utils {
    public static InvalidationListener listen(Observable value, Runnable func) {
        InvalidationListener listener = o -> func.run();
        value.addListener(listener);
        func.run();
        return listener;
    }

    public static <T> InvalidationListener listen(ObservableValue<T> value, Consumer<T> func) {
        return listen(value, () -> func.accept(value.getValue()));
    }

    public static InvalidationListener listen(ObservableFloatValue value, Consumer<Float> func) {
        return listen(value, () -> func.accept(value.get()));
    }

    public static InvalidationListener listen(ObservableIntegerValue value, Consumer<Integer> func) {
        return listen(value, () -> func.accept(value.get()));
    }

    public static Vector3f color(Color color) {
        float r = (float)color.getRed();
        float g = (float)color.getGreen();
        float b = (float)color.getBlue();
        return new Vector3f(r, g, b);
    }

    public static <T> StringConverter<T> stringConverter(Function<T, String> function) {
        return new StringConverter<>() {
            @Override
            public String toString(T object) {
                return function.apply(object);
            }

            @Override
            public T fromString(String s) {
                return null;
            }
        };
    }
}
