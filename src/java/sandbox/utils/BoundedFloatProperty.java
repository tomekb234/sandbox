package sandbox.utils;

import javafx.beans.property.SimpleFloatProperty;

// utility class for bounded numeric properties
public class BoundedFloatProperty extends SimpleFloatProperty {
    public final boolean zero;
    public final float min;
    public final float max;

    public BoundedFloatProperty(float value, boolean zero, float min, float max) {
        super(value);
        this.zero = zero;
        this.min = min;
        this.max = max;
    }

    public BoundedFloatProperty(float value, boolean zero) {
        this(value, zero, 0, Float.POSITIVE_INFINITY);
    }

    @Override
    public void set(float value) {
        if ((value != 0 || zero) && value >= min && value <= max)
            super.set(value);
    }
}
