package sandbox.utils;

import javafx.beans.property.SimpleObjectProperty;

import java.util.function.Consumer;
import java.util.function.Supplier;

// utility class for properties changing when the simulation is unpaused (position, speed, etc.)
public class VolatileProperty<T> extends SimpleObjectProperty<T> {
    private Supplier<T> read;
    private Consumer<T> write;

    public VolatileProperty(Supplier<T> read, Consumer<T> write) {
        this.read = read;
        this.write = write;
    }

    public void update() {
        super.set(read.get());
    }

    @Override
    public void set(T value) {
        super.set(value);
        write.accept(value);
    }
}
