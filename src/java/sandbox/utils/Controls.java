package sandbox.utils;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import sandbox.gui.Icons;
import sandbox.gui.Language;

import java.util.function.Function;
import java.util.function.UnaryOperator;

// utility GUI functions
public class Controls {
    public static final RegexFilter integerFilter = new RegexFilter("-?\\d*");
    public static final RegexFilter floatFilter = new RegexFilter("-?\\d*(\\.\\d*)?");
    public static final NumberConverter integerConverter = new NumberConverter("%d", Integer::parseInt);
    public static final NumberConverter floatConverter = new NumberConverter("%.2f", Float::parseFloat);

    public static Label label(String key, ObservableValue<?>... args) {
        var label = new Label();
        label.textProperty().bind(Language.get(key, args));
        var icon = Icons.get(key);
        if (icon != null)
            label.setGraphic(new ImageView(icon));
        return label;
    }

    public static Label icon(String key, ObservableValue<?>... args) {
        var label = new Label();
        label.setGraphic(new ImageView(Icons.getLarge(key)));
        var tooltip = new Tooltip();
        tooltip.textProperty().bind(Language.get(key, args));
        label.setTooltip(tooltip);
        return label;
    }

    public static Button button(String key, ObservableValue<?>... args) {
        var button = new Button();
        var icon = Icons.getLarge(key);
        if (icon != null) {
            button.setGraphic(new ImageView(icon));
            var tooltip = new Tooltip();
            tooltip.textProperty().bind(Language.get(key, args));
            button.setTooltip(tooltip);
        } else
            button.textProperty().bind(Language.get(key, args));
        return button;
    }

    public static CheckBox checkBox(String key, ObservableValue<?>... args) {
        var checkBox = new CheckBox();
        checkBox.textProperty().bind(Language.get(key, args));
        var icon = Icons.get(key);
        if (icon != null)
            checkBox.setGraphic(new ImageView(icon));
        return checkBox;
    }

    public static Menu menu(String key, ObservableValue<?>... args) {
        var menu = new Menu();
        menu.textProperty().bind(Language.get(key, args));
        var icon = Icons.get(key);
        if (icon != null)
            menu.setGraphic(new ImageView(icon));
        return menu;
    }

    public static MenuItem menuItem(String key, ObservableValue<?>... args) {
        var item = new MenuItem();
        item.textProperty().bind(Language.get(key, args));
        var icon = Icons.get(key);
        if (icon != null)
            item.setGraphic(new ImageView(icon));
        return item;
    }

    public static CheckMenuItem checkMenuItem(String key, ObservableValue<?>... args) {
        var item = new CheckMenuItem();
        item.textProperty().bind(Language.get(key, args));
        var icon = Icons.get(key);
        if (icon != null)
            item.setGraphic(new ImageView(icon));
        return item;
    }

    public static RadioMenuItem radioMenuItem(String key, ObservableValue<?>... args) {
        var item = new RadioMenuItem();
        item.textProperty().bind(Language.get(key, args));
        var icon = Icons.get(key);
        if (icon != null)
            item.setGraphic(new ImageView(icon));
        return item;
    }

    public static class RegexFilter implements UnaryOperator<TextFormatter.Change> {
        private String regex;

        public RegexFilter(String regex) {
            this.regex = regex;
        }

        @Override
        public TextFormatter.Change apply(TextFormatter.Change change) {
            if (change.getControlNewText().matches(regex))
                return change;
            return null;
        }
    }

    // JavaFX's NumberStringConverter throws exceptions instead of ignoring invalid values
    public static class NumberConverter extends StringConverter<Number> {
        private String format;
        private Function<String, Number> parse;

        public NumberConverter(String format, Function<String, Number> parse) {
            this.format = format;
            this.parse = parse;
        }

        @Override
        public String toString(Number number) {
            return String.format(format, number);
        }

        @Override
        public Number fromString(String string) {
            try {
                return parse.apply(string);
            } catch (NumberFormatException ignored) {
                return null;
            }
        }
    }
}
