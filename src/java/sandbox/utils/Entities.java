package sandbox.utils;

import sandbox.Sandbox;
import sandbox.entity.Entity;

// utility functions for the entity tree
public class Entities {
    public static void attachAll(Entity entity, Sandbox sandbox) {
        for (var child : entity.children())
            attachAll(child, sandbox);
        entity.attach(sandbox);
    }

    public static void detachAll(Entity entity, Sandbox sandbox) {
        entity.detach(sandbox);
        for (var child : entity.children())
            detachAll(child, sandbox);
    }

    public static void removeAll(Entity entity) {
        entity.remove();
        for (var child : entity.children())
            removeAll(child);
    }

    public static <T extends Entity> T copyAll(T entity) {
        var copy = Properties.copy(entity);
        for (var child : entity.children())
            copy.children().add(copyAll(child));
        return copy;
    }

    public static Entity find(Entity entity, int id) {
        if (entity.id() == id)
            return entity;
        for (var child : entity.children()) {
            var result = find(child, id);
            if (result != null)
                return result;
        } return null;
    }

    public static boolean ancestor(Entity entity, Entity ancestor) {
        if (entity == ancestor)
            return true;
        for (var child : ancestor.children()) {
            if (ancestor(entity, child))
                return true;
        } return false;
    }

    public static Entity parent(Entity entity, Entity ancestor) {
        if (ancestor.children().contains(entity))
            return ancestor;
        for (var child : ancestor.children()) {
            var result = parent(entity, child);
            if (result != null)
                return result;
        } return null;
    }
}
