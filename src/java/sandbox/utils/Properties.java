package sandbox.utils;

import javafx.beans.property.Property;
import javafx.scene.paint.Color;
import sandbox.engine.Vector3f;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// utility functions for entity properties (using reflection)
public class Properties {
    // list of all properties (derived first)
    public static List<Field> properties(Class<?> cls) {
        var sup = cls.getSuperclass();
        var fields = sup != null ? properties(sup) : new ArrayList<Field>();
        for (var field : cls.getDeclaredFields()) {
            if (!Modifier.isPublic(field.getModifiers()))
                continue;
            if (!Property.class.isAssignableFrom(field.getType()))
                continue;
            fields.add(field);
        } return fields;
    }

    // update all volatile properties
    @SuppressWarnings("unchecked")
    public static <T> void update(T object) {
        try {
            var cls = object.getClass();
            for (var field : properties(cls)) {
                if (!VolatileProperty.class.isAssignableFrom(field.getType()))
                    continue;
                var property = (VolatileProperty<Object>)field.get(object);
                property.update();
            }
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    // copy all properties to another object
    @SuppressWarnings("unchecked")
    public static <T> void copy(T object, T copy) {
        try {
            update(object);
            var cls = object.getClass();
            for (var field : properties(cls)) {
                var property = (Property<Object>)field.get(object);
                var copyProperty = (Property<Object>)field.get(copy);
                copyProperty.setValue(property.getValue());
            }
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    // create new object of the same type with copied properties
    @SuppressWarnings("unchecked")
    public static <T> T copy(T object) {
        try {
            var cls = object.getClass();
            var copy = cls.getConstructor().newInstance();
            copy(object, copy);
            return (T)copy;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    // used by XML reader
    @SuppressWarnings("unchecked")
    public static void read(Object object, Map<String, String> values) throws Exception {
        var cls = object.getClass();
        for (var entry : values.entrySet()) {
            var field = cls.getField(entry.getKey());
            var property = (Property<Object>)field.get(object);
            var value = readValue(entry.getValue());
            property.setValue(value);
        }
    }

    // used by XML writer
    @SuppressWarnings("unchecked")
    public static Map<String, String> write(Object object) {
        try {
            update(object);
            var values = new HashMap<String, String>();
            var cls = object.getClass();
            for (var field : properties(cls)) {
                var property = (Property<Object>)field.get(object);
                var value = writeValue(property.getValue());
                values.put(field.getName(), value);
            } return values;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object readValue(String string) throws Exception {
        var separator = string.indexOf(':');
        var type = string.substring(0, separator);
        var value = string.substring(separator + 1);
        if (type.equals("string"))
            return value;
        if (type.equals("boolean"))
            return Boolean.valueOf(value);
        if (type.equals("int"))
            return Integer.valueOf(value);
        if (type.equals("float"))
            return Float.valueOf(value);
        if (type.equals("vector"))
            return Vector3f.valueOf(value);
        if (type.equals("color"))
            return Color.valueOf(value);
        throw new Exception("invalid value " + string);
    }

    public static String writeValue(Object value) {
        if (value instanceof String)
            return "string:" + value;
        if (value instanceof Boolean)
            return "boolean:" + value;
        if (value instanceof Integer)
            return "int:" + value;
        if (value instanceof Float)
            return "float:" + value;
        if (value instanceof Vector3f)
            return "vector:" + value;
        if (value instanceof Color)
            return "color:" + value;
        throw new RuntimeException();
    }
}
