package sandbox.files;

import sandbox.engine.Mesh;
import sandbox.engine.Vector3f;

import java.io.*;
import java.util.*;

public class ModelLoader {
    private List<Element> elements;
    private List<Vertex> vertices;
    private List<List<Integer>> faces;

    public void loadPLY(File file) throws Exception {
        try (var stream = new FileInputStream(file)) {
            loadPLY(stream);
        }
    }

    public void loadPLY(InputStream stream) throws Exception {
        elements = new ArrayList<>();
        vertices = new ArrayList<>();
        faces = new ArrayList<>();

        var reader = new BufferedReader(new InputStreamReader(stream));
        if (!Objects.equals(reader.readLine(), "ply"))
            throw new Exception("invalid file header");
        if (!Objects.equals(reader.readLine(), "format ascii 1.0"))
            throw new Exception("unsupported file format");

        readHeader(reader);
        readElements(reader);
    }

    public Mesh build() throws Exception {
        var vertexData = new ArrayList<Float>();
        var normalData = new ArrayList<Float>();
        var colorData = new ArrayList<Float>();
        var size = 0;

        for (var face : faces) {
            if (face.size() != 3)
                throw new Exception("not triangulated");
            for (var i : face) {
                if (i < 0 || i >= vertices.size())
                    throw new Exception("invalid vertex index " + i);
                var vertex = vertices.get(i);
                vertexData.add(vertex.x);
                vertexData.add(vertex.y);
                vertexData.add(vertex.z);
                normalData.add(vertex.nx);
                normalData.add(vertex.ny);
                normalData.add(vertex.nz);
                colorData.add(vertex.r);
                colorData.add(vertex.g);
                colorData.add(vertex.b);
                size++;
            }
        }

        var mesh = new Mesh(Mesh.shapeTypeConvexHull, Vector3f.zero, size);
        mesh.loadVertices(toFloatArray(vertexData));
        mesh.loadNormals(toFloatArray(normalData));
        mesh.loadColors(toFloatArray(colorData));
        return mesh;
    }

    private void readHeader(BufferedReader reader) throws Exception {
        Element element = null;
        var end = false;

        while (!end) {
            var line = reader.readLine();
            if (line == null)
                throw new Exception("unexpected end of file");
            var scanner = new Scanner(line);

            switch (scanner.next()) {
                case "comment":
                    continue;

                case "element": {
                    var name = scanner.next();
                    var size = scanner.nextInt();
                    element = new Element(name, size);
                    elements.add(element);
                    break;
                }

                case "property": {
                    if (element == null)
                        throw new Exception("property without element");
                    var type = scanner.next();
                    if (type.equals("list")) {
                        scanner.next();
                        scanner.next();
                    } var name = scanner.next();
                    element.properties.add(new Element.Property(name, type.equals("list")));
                    break;
                }

                case "end_header":
                    end = true;
                    break;
            }
        }
    }

    private void readElements(BufferedReader reader) throws Exception {
        for (var element : elements) {
            for (var i = 0; i < element.size; i++) {
                var line = reader.readLine();
                if (line == null)
                    throw new Exception("unexpected end of file");

                if (element.name.equals("vertex")) {
                    var scanner = new Scanner(line);
                    var vertex = new Vertex();
                    vertices.add(vertex);
                    for (var property : element.properties)
                        readVertexProperty(scanner, property, vertex);
                }

                else if (element.name.equals("face")) {
                    var scanner = new Scanner(line);
                    var face = new ArrayList<Integer>();
                    faces.add(face);
                    for (var property : element.properties)
                        readFaceProperty(scanner, property, face);
                }
            }
        }
    }

    private void readVertexProperty(Scanner scanner, Element.Property property, Vertex vertex) {
        switch (property.name) {
            case "x": vertex.x = scanner.nextFloat(); break;
            case "y": vertex.y = scanner.nextFloat(); break;
            case "z": vertex.z = scanner.nextFloat(); break;
            case "nx": vertex.nx = scanner.nextFloat(); break;
            case "ny": vertex.ny = scanner.nextFloat(); break;
            case "nz": vertex.nz = scanner.nextFloat(); break;
            case "red": vertex.r = (float)scanner.nextInt() / 255; break;
            case "green": vertex.g = (float)scanner.nextInt() / 255; break;
            case "blue": vertex.b = (float)scanner.nextInt() / 255; break;
            default: skipProperty(scanner, property); break;
        }
    }

    private void readFaceProperty(Scanner scanner, Element.Property property, List<Integer> face) {
        if (property.name.equals("vertex_index") || property.name.equals("vertex_indices")) {
            var size = scanner.nextInt();
            for (var i = 0; i < size; i++)
                face.add(scanner.nextInt());
        } else
            skipProperty(scanner, property);
    }

    private void skipProperty(Scanner scanner, Element.Property property) {
        if (property.list) {
            var size = scanner.nextInt();
            for (var i = 0; i < size; i++)
                scanner.next();
        } else
            scanner.next();
    }

    private static float[] toFloatArray(List<Float> list) {
        var array = new float[list.size()];
        for (var i = 0; i < list.size(); i++)
            array[i] = list.get(i);
        return array;
    }

    private static class Element {
        public final String name;
        public final int size;
        public final List<Property> properties;

        public Element(String name, int size) {
            this.name = name;
            this.size = size;
            properties = new ArrayList<>();
        }

        public static class Property {
            public final String name;
            public final boolean list;

            public Property(String name, boolean list) {
                this.name = name;
                this.list = list;
            }
        }
    }

    private static class Vertex {
        public float x, y, z;
        public float nx = 0f, ny = 0f, nz = 0f;
        public float r = 0.5f, g = 0.5f, b = 0.5f;
    }
}
