package sandbox.files;

import sandbox.entity.Entity;
import sandbox.utils.Properties;

import javax.xml.stream.*;
import java.io.*;

public class EntityWriter {
    private static XMLOutputFactory factory = XMLOutputFactory.newInstance();

    public static void writeXML(File file, Entity entity) throws IOException, XMLStreamException {
        try (var stream = new FileOutputStream(file)) {
            writeXML(stream, entity);
        }
    }

    public static void writeXML(OutputStream stream, Entity entity) throws XMLStreamException {
        var writer = factory.createXMLStreamWriter(stream);
        writer.writeStartDocument();
        writer.writeCharacters("\n");
        writeXML(writer, entity);
        writer.writeEndDocument();
    }

    private static void writeXML(XMLStreamWriter writer, Entity entity) throws XMLStreamException {
        var name = entity.getClass().getName();
        if (entity.group())
            writer.writeStartElement(name);
        else
            writer.writeEmptyElement(name);

        var values = Properties.write(entity);
        for (var entry : values.entrySet())
            writer.writeAttribute(entry.getKey(), entry.getValue());
        writer.writeCharacters("\n");

        for (var child : entity.children())
            writeXML(writer, child);

        if (entity.group()) {
            writer.writeEndElement();
            writer.writeCharacters("\n");
        }
    }
}
