package sandbox.files;

import sandbox.entity.Entity;
import sandbox.utils.Properties;

import javax.xml.stream.XMLInputFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Stack;

public class EntityReader {
    private static XMLInputFactory factory = XMLInputFactory.newInstance();

    public static Entity readXML(File file) throws Exception {
        try (var stream = new FileInputStream(file)) {
            return readXML(stream);
        }
    }

    public static Entity readXML(InputStream stream) throws Exception {
        var reader = factory.createXMLEventReader(stream);
        var stack = new Stack<Entity>();

        while (reader.hasNext()) {
            var event = reader.nextTag();

            if (event.isStartElement()) {
                var element = event.asStartElement();
                var cls = Class.forName(element.getName().getLocalPart());
                var entity = (Entity)cls.getConstructor().newInstance();

                if (!stack.empty())
                    stack.peek().children().add(entity);
                stack.push(entity);

                var values = new HashMap<String, String>();
                var attributes = element.getAttributes();
                while (attributes.hasNext()) {
                    var attribute = attributes.next();
                    values.put(attribute.getName().getLocalPart(), attribute.getValue());
                } Properties.read(entity, values);
            }

            else if (event.isEndElement()) {
                var entity = stack.pop();
                if (stack.empty())
                    return entity;
            }
        }

        throw new Exception("no root");
    }
}
