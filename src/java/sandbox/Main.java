package sandbox;

import javafx.application.Application;
import javafx.stage.Stage;
import sandbox.gui.Language;
import sandbox.gui.MainScene;

import java.io.File;

// program entry point
public class Main extends Application {
    static {
        // loading native library
        var lib = new File(System.getProperty("sandbox.lib"));
        System.load(lib.getAbsolutePath());
    }

    private Sandbox sandbox;

    @Override
    public void init() throws Exception {
        super.init();
        sandbox = new Sandbox();
    }

    @Override
    public void start(Stage stage) {
        var scene = new MainScene(sandbox);
        stage.setScene(scene.scene);
        stage.titleProperty().bind(Language.get("title"));
        stage.show();
        sandbox.start(); // must be called after stage.show()
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        sandbox.stop();
    }

    public static void main(String[] args) {
        launch();
    }
}
