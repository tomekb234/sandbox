package sandbox;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sandbox.engine.*;
import sandbox.entity.*;
import sandbox.entity.light.DirectionalLight;
import sandbox.entity.light.PointLight;
import sandbox.entity.shape.*;
import sandbox.files.ModelLoader;
import sandbox.utils.Entities;
import sandbox.utils.Utils;
import sandbox.files.EntityReader;
import sandbox.files.EntityWriter;

import java.io.File;
import java.util.*;
import java.util.function.Supplier;

// central class controlling the simulation
public class Sandbox {
    public final GraphicEngine graphics;
    public final PhysicsEngine physics;
    public final ObjectProperty<World> world;
    public final BooleanProperty paused;
    public final FloatProperty speed;
    public final BooleanProperty revertible;
    private Stack<World> states;
    private Map<String, Mesh> models;

    public Sandbox() {
        graphics = new GraphicEngine();
        physics = new PhysicsEngine();
        world = new SimpleObjectProperty<>();
        paused = new SimpleBooleanProperty(true);
        speed = new SimpleFloatProperty(1);
        revertible = new SimpleBooleanProperty();
        states = new Stack<>();
        models = new HashMap<>();

        Utils.listen(paused, physics::setPaused);
        Utils.listen(speed, physics::setSpeed);

        registerMesh(Cube.mesh);
        registerMesh(Sphere.mesh);
        registerMesh(Cone.mesh);
        registerMesh(Cylinder.mesh);

        newWorld();
    }

    public void start() {
        graphics.start();
        physics.start();
    }

    public void stop() {
        graphics.stop();
        physics.stop();
    }

    public void newWorld(World newWorld) {
        pause();
        states.clear();
        revertible.set(false);
        var oldWorld = world.get();
        if (oldWorld != null) {
            Entities.detachAll(oldWorld, this);
            Entities.removeAll(oldWorld);
        } Entities.attachAll(newWorld, this);
        world.set(newWorld);
    }

    public void newWorld() {
        newWorld(new World("World"));
    }

    public void resume() {
        var copy = Entities.copyAll(world.get());
        states.push(copy);
        revertible.set(true);
        paused.set(false);
    }

    public void pause() {
        paused.set(true);
    }

    public void revert() {
        if (states.empty())
            return;
        pause();
        var oldWorld = world.get();
        var newWorld = states.pop();
        revertible.set(!states.empty());
        Entities.detachAll(oldWorld, this);
        Entities.removeAll(oldWorld);
        Entities.attachAll(newWorld, this);
        world.set(newWorld);
    }

    public void open(File file) throws Exception {
        newWorld((World)EntityReader.readXML(file));
    }

    public void save(File file) throws Exception {
        EntityWriter.writeXML(file, world.get());
    }

    public void add(Entity entity, Entity parent) {
        if (!parent.group())
            return;
        parent.children().add(entity);
        Entities.attachAll(entity, this);
    }

    public void add(Entity entity) {
        add(entity, world.get());
    }

    public void remove(Entity entity) {
        var parent = Entities.parent(entity, world.get());
        if (parent == null || !parent.group())
            return;
        parent.children().remove(entity);
        Entities.detachAll(entity, this);
        Entities.removeAll(entity);
    }

    public void move(Entity entity, Entity newParent) {
        var parent = Entities.parent(entity, world.get());
        if (parent == null || !parent.group() || !newParent.group())
            return;
        parent.children().remove(entity);
        newParent.children().add(entity);
    }

    public void registerMesh(Mesh mesh) {
        graphics.registerMesh(mesh);
        physics.registerMesh(mesh);
    }

    public void loadModel(File file) throws Exception {
        var name = file.getName();
        if (models.containsKey(name))
            return;
        var loader = new ModelLoader();
        loader.loadPLY(file);
        var mesh = loader.build();
        registerMesh(mesh);
        models.put(name, mesh);
        modelFactories.add(new ModelFactory(name));
    }

    public Mesh getModel(String name) {
        return models.getOrDefault(name, null);
    }

    public Vector3f cameraDirection() {
        return graphics.getCameraDirection();
    }

    public Vector3f positionBeforeCamera() {
        var position = graphics.getCameraPosition();
        var direction = graphics.getCameraDirection();
        return position.add(direction.multiply(4));
    }

    // entity adding utilities
    public class EntityFactory<T extends Entity> {
        public final String type;
        private Supplier<T> supplier;
        private int count;

        public EntityFactory(String type, Supplier<T> supplier) {
            this.type = type;
            this.supplier = supplier;
            count = 0;
        }

        public T addNew() {
            var entity = supplier.get();
            entity.name.set(entity.name.get() + " " + (++count));
            add(entity);
            return entity;
        }
    }

    public class ModelFactory extends EntityFactory<Model> {
        public final String model;

        public ModelFactory(String model) {
            super("model", () -> new Model(model, model, positionBeforeCamera()));
            this.model = model;
        }
    }

    public final EntityFactory<Group> groupFactory = new EntityFactory<>("group", () -> new Group("Group"));

    public final List<EntityFactory<?>> factories = List.of(
        groupFactory,
        new EntityFactory<>("cube", () -> new Cube("Cube", positionBeforeCamera())),
        new EntityFactory<>("sphere", () -> new Sphere("Sphere", positionBeforeCamera())),
        new EntityFactory<>("cone", () -> new Cone("Cone", positionBeforeCamera())),
        new EntityFactory<>("cylinder", () -> new Cylinder("Cylinder", positionBeforeCamera())),
        new EntityFactory<>("point-light", () -> new PointLight("Point light", positionBeforeCamera())),
        new EntityFactory<>("directional-light", () -> new DirectionalLight("Directional light", cameraDirection())),
        new EntityFactory<>("rope", () -> new Rope("Rope"))
    );

    public final ObservableList<ModelFactory> modelFactories = FXCollections.observableArrayList();
}
