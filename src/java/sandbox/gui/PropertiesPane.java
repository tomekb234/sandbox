package sandbox.gui;

import javafx.animation.AnimationTimer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import sandbox.utils.*;
import sandbox.engine.Vector3f;
import sandbox.entity.Entity;

import java.lang.reflect.Modifier;

// properties editor for selected entity
public class PropertiesPane extends VBox {
    public final ObjectProperty<Entity> current;
    public final BooleanProperty paused;

    public PropertiesPane() {
        current = new SimpleObjectProperty<>();
        paused = new SimpleBooleanProperty();
        Utils.listen(current, this::build);

        setSpacing(10);
        setPadding(new Insets(5));
        new Timer().start();
    }

    // updating volatile properties when the simulation is unpaused
    private class Timer extends AnimationTimer {
        private static final long interval = 100_000_000; // 100ms
        private long next = 0;

        @Override
        public void handle(long time) {
            if (time < next || paused.get())
                return;
            next = time + interval;
            var entity = current.get();
            if (entity != null)
                Properties.update(entity);
        }
    }

    private void build() {
        getChildren().clear();
        var entity = current.get();
        if (entity == null)
            return;

        var label = Controls.label(entity.type());
        label.setFont(Font.font(null, FontWeight.BOLD, -1));
        getChildren().add(label);

        // using reflection to match entity properties with Item objects
        for (var field : Properties.properties(entity.getClass())) {
            var name = field.getName();
            var type = field.getGenericType();

            for (var method : Item.class.getDeclaredMethods()) {
                if (!Modifier.isPublic(method.getModifiers()))
                    continue;
                if (!Modifier.isStatic(method.getModifiers()))
                    continue;
                if (!method.getParameterTypes()[0].equals(String.class))
                    continue;
                if (!method.getGenericParameterTypes()[1].equals(type))
                    continue;

                try {
                    var property = field.get(entity);
                    var item = (Item)method.invoke(null, name, property);
                    item.paused.bind(paused);
                    getChildren().add(item);
                    break;
                } catch (ReflectiveOperationException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    // single property editor
    @SuppressWarnings("unused")
    private static class Item extends VBox {
        public final BooleanProperty paused;

        private Item(String name, Node content, boolean pausedOnly) {
            paused = new SimpleBooleanProperty();
            if (pausedOnly)
                content.disableProperty().bind(Bindings.not(paused));
            if (name != null)
                getChildren().add(Controls.label(name));
            getChildren().add(content);
            setSpacing(5);
        }

        private Item(String name, Node content) {
            this(name, content, false);
        }

        public static Item booleanItem(String name, BooleanProperty property) {
            var checkBox = Controls.checkBox(name);
            checkBox.selectedProperty().bindBidirectional(property);
            return new Item(null, checkBox);
        }

        public static Item stringItem(String name, StringProperty property) {
            var field = new TextField();
            field.textProperty().bindBidirectional(property);
            return new Item(name, field);
        }

        public static Item integerItem(String name, IntegerProperty property) {
            var field = new TextField();
            field.setTextFormatter(new TextFormatter<>(Controls.integerFilter));
            field.textProperty().bindBidirectional(property, Controls.integerConverter);
            return new Item(name, field);
        }

        public static Item floatItem(String name, FloatProperty property) {
            var field = new TextField();
            field.setTextFormatter(new TextFormatter<>(Controls.floatFilter));
            field.textProperty().bindBidirectional(property, Controls.floatConverter);
            return new Item(name, field);
        }

        public static Item boundedFloatItem(String name, BoundedFloatProperty property) {
            if (Float.isInfinite(property.min) || Float.isInfinite(property.max))
                return floatItem(name, property);
            var slider = new Slider(property.min, property.max, property.get());
            slider.valueProperty().bindBidirectional(property);
            return new Item(name, slider);
        }

        public static Item vectorItem(String name, ObjectProperty<Vector3f> property) {
            var editor = new VectorEditor();
            editor.vector.bindBidirectional(property);
            return new Item(name, editor);
        }

        public static Item volatileVectorItem(String name, VolatileProperty<Vector3f> property) {
            var editor = new VectorEditor();
            property.update();
            editor.vector.bindBidirectional(property);
            return new Item(name, editor, true);
        }

        public static Item colorItem(String name, ObjectProperty<Color> property) {
            var picker = new ColorPicker();
            picker.valueProperty().bindBidirectional(property);
            picker.setMinWidth(100);
            picker.setMinHeight(25);
            return new Item(name, picker);
        }
    }
}
