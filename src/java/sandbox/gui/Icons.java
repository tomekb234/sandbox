package sandbox.gui;

import javafx.scene.image.Image;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Icons {
    private static Properties icons;
    private static Properties largeIcons;
    private static Map<String, Image> images;
    private static Map<String, Image> largeImages;

    static {
        icons = new Properties();
        largeIcons = new Properties();
        images = new HashMap<>();
        largeImages = new HashMap<>();

        try {
            icons.load(stream("icons.properties"));
            largeIcons.load(stream("icons-large.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Image get(String key) {
        if (!icons.containsKey(key))
            return null;
        var file = icons.getProperty(key);
        if (!images.containsKey(file))
            images.put(file, new Image(stream("icons/" + file)));
        return images.get(file);
    }

    public static Image getLarge(String key) {
        if (!largeIcons.containsKey(key))
            return null;
        var file = largeIcons.getProperty(key);
        if (!largeImages.containsKey(file))
            largeImages.put(file, new Image(stream("icons/" + file)));
        return largeImages.get(file);
    }

    private static InputStream stream(String file) {
        return ClassLoader.getSystemResourceAsStream(file);
    }
}
