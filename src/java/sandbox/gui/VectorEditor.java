package sandbox.gui;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import sandbox.engine.Vector3f;
import sandbox.utils.Controls;

public class VectorEditor extends GridPane {
    public final ObjectProperty<Vector3f> vector;
    public final FloatProperty x;
    public final FloatProperty y;
    public final FloatProperty z;
    private boolean updating;

    public VectorEditor() {
        vector = new SimpleObjectProperty<>(Vector3f.zero);
        x = new SimpleFloatProperty(0);
        y = new SimpleFloatProperty(0);
        z = new SimpleFloatProperty(0);
        updating = false;

        vector.addListener(o -> updateAxes());
        x.addListener(o -> updateVector());
        y.addListener(o -> updateVector());
        z.addListener(o -> updateVector());

        var xField = new TextField();
        xField.setTextFormatter(new TextFormatter<>(Controls.floatFilter));
        xField.textProperty().bindBidirectional(x, Controls.floatConverter);
        add(new Label("X"), 0, 0);
        add(xField, 1, 0);
        GridPane.setHgrow(xField, Priority.ALWAYS);

        var yField = new TextField();
        yField.setTextFormatter(new TextFormatter<>(Controls.floatFilter));
        yField.textProperty().bindBidirectional(y, Controls.floatConverter);
        add(new Label("Y"), 0, 1);
        add(yField, 1, 1);
        GridPane.setHgrow(yField, Priority.ALWAYS);

        var zField = new TextField();
        zField.setTextFormatter(new TextFormatter<>(Controls.floatFilter));
        zField.textProperty().bindBidirectional(z, Controls.floatConverter);
        add(new Label("Z"), 0, 2);
        add(zField, 1, 2);
        GridPane.setHgrow(zField, Priority.ALWAYS);

        setHgap(5);
        setVgap(5);
    }

    private void updateVector() {
        if (updating)
            return;
        updating = true;
        vector.set(new Vector3f(x.get(), y.get(), z.get()));
        updating = false;
    }

    private void updateAxes() {
        if (updating)
            return;
        updating = true;
        x.set(vector.get().x);
        y.set(vector.get().y);
        z.set(vector.get().z);
        updating = false;
    }
}
