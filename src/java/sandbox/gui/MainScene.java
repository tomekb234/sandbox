package sandbox.gui;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import sandbox.Sandbox;
import sandbox.engine.Vector3f;
import sandbox.entity.Entity;
import sandbox.entity.World;
import sandbox.utils.Entities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

// main GUI layout
public class MainScene {
    private static final int initialWidth = 1000;
    private static final int initialHeight = 600;

    public final Scene scene;
    public final ObjectProperty<Entity> selection;
    public final ObjectProperty<List<Entity>> multipleSelection;
    private Sandbox sandbox;
    private ObjectProperty<World> world;

    public MainScene(Sandbox sandbox) {
        this.sandbox = sandbox;
        world = new SimpleObjectProperty<>();
        selection = new SimpleObjectProperty<>();
        multipleSelection = new SimpleObjectProperty<>(List.of());

        world.bind(sandbox.world);
        world.addListener(o -> multipleSelection.set(List.of()));

        multipleSelection.addListener((o, oldList, newList) -> {
            for (var entity : oldList)
                entity.select(false);
            for (var entity : newList)
                entity.select(true);
        });

        var menuBar = new MainMenuBar(sandbox, this);
        var toolBar = new MainToolBar(sandbox, this);
        var statusBar = new StatusBar(sandbox);

        var outline = new OutlineTree(sandbox);
        var outlineToolBar = new OutlineToolBar(this);
        outline.selection.bindBidirectional(selection);
        outline.multipleSelection.bindBidirectional(multipleSelection);

        var properties = new PropertiesPane();
        properties.current.bind(selection);
        properties.paused.bind(sandbox.paused);

        var viewport = new Viewport(sandbox.graphics, this);
        viewport.fpsLimit.bindBidirectional(menuBar.fpsLimit);
        viewport.ownWindow.bindBidirectional(menuBar.ownWindow);

        var outlinePane = new BorderPane();
        outlinePane.setCenter(scrollable(outline));
        outlinePane.setBottom(outlineToolBar);

        var center = new SplitPane();
        center.getItems().add(outlinePane);
        center.getItems().add(viewport);
        center.getItems().add(scrollable(properties));
        center.setDividerPositions(0.18, 0.82);

        var root = new BorderPane();
        root.setTop(new VBox(menuBar, toolBar));
        root.setCenter(center);
        root.setBottom(statusBar);

        scene = new Scene(root, initialWidth, initialHeight);
    }

    public boolean allowRemove() {
        var selection = multipleSelection.get();
        return !selection.isEmpty() && !selection.contains(world.get());
    }

    public boolean allowGroup() {
        var selection = multipleSelection.get();
        return selection.size() > 1 && !selection.contains(world.get());
    }

    public void remove() {
        if (!allowRemove())
            return;
        var selection = multipleSelection.get();
        for (var entity : selection)
            sandbox.remove(entity);
        multipleSelection.set(List.of());
    }

    public void group() {
        if (!allowGroup())
            return;
        var selection = multipleSelection.get();
        var group = sandbox.groupFactory.addNew();
        for (var entity : selection)
            sandbox.move(entity, group);
        this.selection.set(group);
    }

    public void select(boolean multiple, Vector3f pos, Vector3f dir) {
        var id = sandbox.physics.rayCast(pos, dir);
        var entity = (id != -1) ? Entities.find(world.get(), id) : null;
        if (entity != null) {
            if (multiple) {
                var list = new ArrayList<>(multipleSelection.get());
                if (list.contains(entity))
                    list.remove(entity);
                else
                    list.add(entity);
                multipleSelection.set(list);
            } else
                selection.set(entity);
        } else
            multipleSelection.set(List.of());
    }

    public void open() {
        try {
            var file = chooseFile("open", "xml");
            if (file != null)
                sandbox.open(file);
        } catch (Exception e) {
            errorAlert(e);
        }
    }

    public void save() {
        try {
            var file = chooseFile("save", "xml");
            if (file != null)
                sandbox.save(file);
        } catch (Exception e) {
            errorAlert(e);
        }
    }

    public void loadModel() {
        try {
            var file = chooseFile("load-model", "ply");
            if (file != null)
                sandbox.loadModel(file);
        } catch (Exception e) {
            errorAlert(e);
        }
    }

    public void about() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.titleProperty().bind(Language.get("about"));
        alert.headerTextProperty().bind(Language.get("about"));
        alert.setContentText(Language.about());
        alert.show();
    }

    private ScrollPane scrollable(Node node) {
        var pane = new ScrollPane(node);
        pane.setFitToWidth(true);
        pane.setFitToHeight(true);
        return pane;
    }

    private File chooseFile(String title, String extension) {
        var dialog = new TextInputDialog();
        dialog.titleProperty().bind(Language.get(title));
        dialog.headerTextProperty().bind(Language.get(title));
        dialog.contentTextProperty().bind(Language.get("file-name-" + extension));
        var result = dialog.showAndWait();
        return result.map(File::new).orElse(null);
    }

    private void errorAlert(Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.titleProperty().bind(Language.get("error"));
        alert.headerTextProperty().bind(Language.get("error"));
        alert.setContentText(e.toString());
        alert.show();
    }
}
