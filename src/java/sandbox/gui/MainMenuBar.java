package sandbox.gui;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import sandbox.Sandbox;
import sandbox.utils.Controls;

public class MainMenuBar extends MenuBar {
    public final IntegerProperty fpsLimit;
    public final BooleanProperty ownWindow;
    private ObservableList<Sandbox.ModelFactory> models;

    public MainMenuBar(Sandbox sandbox, MainScene scene) {
        fpsLimit = new SimpleIntegerProperty(60);
        ownWindow = new SimpleBooleanProperty(false);

        var file = Controls.menu("file");
        getMenus().add(file);

        var newFile = Controls.menuItem("new");
        file.getItems().add(newFile);
        newFile.setOnAction(e -> sandbox.newWorld());

        var open = Controls.menuItem("open");
        file.getItems().add(open);
        open.setOnAction(e -> scene.open());

        var save = Controls.menuItem("save");
        file.getItems().add(save);
        save.setOnAction(e -> scene.save());

        var loadModel = Controls.menuItem("load-model");
        file.getItems().add(loadModel);
        loadModel.setOnAction(e -> scene.loadModel());

        var exit = Controls.menuItem("exit");
        file.getItems().add(exit);
        exit.setOnAction(e -> Platform.exit());

        var edit = Controls.menu("edit");
        getMenus().add(edit);

        var add = Controls.menu("add");
        edit.getItems().add(add);

        for (var factory : sandbox.factories) {
            var item = Controls.menuItem(factory.type);
            item.setOnAction(e -> scene.selection.set(factory.addNew()));
            add.getItems().add(item);
        }

        models = FXCollections.observableArrayList();
        Bindings.bindContent(models, sandbox.modelFactories);

        var addModel = Controls.menu("model");
        add.getItems().add(addModel);
        addModel.disableProperty().bind(Bindings.isEmpty(models));

        models.addListener((Observable o) -> {
            addModel.getItems().clear();
            for (var factory : models) {
                var item = new MenuItem(factory.model);
                item.setOnAction(e -> scene.selection.set(factory.addNew()));
                addModel.getItems().add(item);
            }
        });

        var allowRemove = Bindings.createBooleanBinding(scene::allowRemove, scene.multipleSelection);
        var allowGroup = Bindings.createBooleanBinding(scene::allowGroup, scene.multipleSelection);

        var remove = Controls.menuItem("remove");
        edit.getItems().add(remove);
        remove.disableProperty().bind(Bindings.not(allowRemove));
        remove.setOnAction(e -> scene.remove());

        var group = Controls.menuItem("group-selected");
        edit.getItems().add(group);
        group.disableProperty().bind(Bindings.not(allowGroup));
        group.setOnAction(e -> scene.group());

        var options = Controls.menu("options");
        getMenus().add(options);

        var languageMenu = Controls.menu("language");
        var languageToggleGroup = new ToggleGroup();
        options.getItems().add(languageMenu);

        for (var lang : Language.languages) {
            var item = Controls.radioMenuItem("lang-" + lang);
            item.setToggleGroup(languageToggleGroup);
            if (lang.equals(Language.language.get()))
                item.setSelected(true);
            item.setOnAction(e -> Language.language.set(lang));
            languageMenu.getItems().add(item);
        }

        var fpsLimitMenu = Controls.menu("fps-limit");
        var fpsLimitToggleGroup = new ToggleGroup();
        options.getItems().add(fpsLimitMenu);

        var limits = new int[] { 20, 30, 60 };
        for (var limit : limits) {
            var item = new RadioMenuItem(Integer.toString(limit));
            item.setToggleGroup(fpsLimitToggleGroup);
            if (limit == fpsLimit.get())
                item.setSelected(true);
            item.setOnAction(e -> fpsLimit.set(limit));
            fpsLimitMenu.getItems().add(item);
        }

        var ownWindowMenu = Controls.checkMenuItem("own-window");
        options.getItems().add(ownWindowMenu);
        ownWindow.bind(ownWindowMenu.selectedProperty());

        var help = Controls.menu("help");
        getMenus().add(help);

        var about = Controls.menuItem("about");
        help.getItems().add(about);
        about.setOnAction(e -> scene.about());
    }
}
