package sandbox.gui;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Language {
    public static final String[] languages = { "en", "pl" };
    public static final StringProperty language;

    static {
        var lang = System.getProperty("sandbox.lang", "en");
        language = new SimpleStringProperty(lang);
        // preventing issues with number formatting
        Locale.setDefault(Locale.ROOT);
    }

    // text mapping responding to the change of language or one of the arguments
    public static StringBinding get(String key, ObservableValue<?>... args) {
        var dependencies = Stream.concat(Stream.of(language), Arrays.stream(args));

        Callable<String> func = () -> {
            var locale = Locale.forLanguageTag(language.get());
            var bundle = ResourceBundle.getBundle("gui", locale);
            var values = Arrays.stream(args).map(ObservableValue::getValue).toArray();
            return String.format(bundle.getString(key), values);
        };

        return Bindings.createStringBinding(func, dependencies.toArray(Observable[]::new));
    }

    // the about dialog text
    public static String about() {
        var file = "about_" + language.get() + ".txt";
        var stream = ClassLoader.getSystemResourceAsStream(file);
        assert stream != null;
        var reader = new BufferedReader(new InputStreamReader(stream));
        return reader.lines().collect(Collectors.joining("\n"));
    }
}
