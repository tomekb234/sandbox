package sandbox.gui;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.WeakInvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import sandbox.Sandbox;
import sandbox.entity.Entity;
import sandbox.utils.Entities;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

// entity tree view
public class OutlineTree extends TreeView<Entity> {
    public final ObjectProperty<Entity> selection;
    public final ObjectProperty<List<Entity>> multipleSelection;
    private BiConsumer<Entity, Entity> move;
    private boolean updating;

    public OutlineTree(Sandbox sandbox) {
        selection = new SimpleObjectProperty<>();
        multipleSelection = new SimpleObjectProperty<>(List.of());
        move = sandbox::move;
        updating = false;

        var root = Bindings.createObjectBinding(() -> new Item(sandbox.world.get()), sandbox.world);
        rootProperty().bind(root);
        setCellFactory(t -> new Cell());

        var model = getSelectionModel();
        model.setSelectionMode(SelectionMode.MULTIPLE);

        // synchronizing selection, multipleSelection and tree view's selected items
        selection.addListener(listener(() -> {
            model.clearSelection();
            var entity = selection.get();
            if (entity != null)
                model.select(root.get().find(entity));
            multipleSelection.set(entity != null ? List.of(entity) : List.of());
        }));

        multipleSelection.addListener(listener(() -> {
            model.clearSelection();
            var list = multipleSelection.get();
            for (var entity : list)
                model.select(root.get().find(entity));
            // single selection must be unambigous
            selection.set(list.size() == 1 ? list.get(0) : null);
        }));

        model.selectedItemProperty().addListener(listener(() -> {
            var item = model.getSelectedItem();
            var items = model.getSelectedItems().stream().filter(Objects::nonNull);
            selection.set(item != null && items.count() == 1 ? item.getValue() : null);
        }));

        model.getSelectedItems().addListener(listener(() -> {
            var items = model.getSelectedItems().stream().filter(Objects::nonNull);
            var list = items.map(TreeItem::getValue).collect(Collectors.toList());
            multipleSelection.set(list);
        }));
    }

    // utility function preventing mutual listener calling
    private InvalidationListener listener(Runnable update) {
        return o -> {
            if (updating)
                return;
            updating = true;
            update.run();
            updating = false;
        };
    }

    private static class Item extends TreeItem<Entity> implements InvalidationListener {
        private Map<Entity, Item> childMap = Collections.emptyMap();

        public Item(Entity entity) {
            super(entity);
            // listening to changes in the list of children
            // (using weak reference to remove the listener automatically)
            entity.children().addListener(new WeakInvalidationListener(this));
            build();
            setExpanded(true);
        }

        private void build() {
            var newChildren = new ArrayList<Item>();
            var newChildMap = new HashMap<Entity, Item>();

            for (var entity : getValue().children()) {
                // prevent creating multiple Item objects for the same entity
                var child = childMap.get(entity);
                if (child == null)
                    child = new Item(entity);
                newChildren.add(child);
                newChildMap.put(entity, child);
            }

            getChildren().setAll(newChildren);
            childMap = newChildMap;
        }

        @Override
        public void invalidated(Observable observable) {
            build();
        }

        public Item find(Entity entity) {
            if (getValue() == entity)
                return this;
            for (var child : childMap.values()) {
                var result = child.find(entity);
                if (result != null)
                    return result;
            } return null;
        }
    }

    private class Cell extends TreeCell<Entity> {
        public Cell() {
            setOnDragDetected(this::dragDetected);
            setOnDragOver(this::dragOver);
            setOnDragDropped(this::dragDropped);
        }

        @Override
        protected void updateItem(Entity item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                textProperty().bind(item.name);
                setGraphic(new ImageView(Icons.get(item.type())));
            } else {
                textProperty().unbind();
                setText(null);
                setGraphic(null);
            }
        }

        private void dragDetected(MouseEvent event) {
            if (isEmpty())
                return;
            var db = startDragAndDrop(TransferMode.MOVE);
            var content = new ClipboardContent();
            content.putString(getItem().toString());
            db.setContent(content);
            db.setDragView(snapshot(null, null));
        }

        private boolean allowMove() {
            if (isEmpty())
                return false;
            var item = getItem();
            if (!item.group())
                return false;
            // prevent moving entity to its descendant
            for (var entity : multipleSelection.get()) {
                if (Entities.ancestor(item, entity))
                    return false;
            } return true;
        }

        private void dragOver(DragEvent event) {
            if (allowMove())
                event.acceptTransferModes(TransferMode.MOVE);
        }

        private void dragDropped(DragEvent event) {
            if (!allowMove())
                return;
            var item = getItem();
            for (var entity : multipleSelection.get())
                move.accept(entity, item);
            selection.set(item);
        }
    }
}
