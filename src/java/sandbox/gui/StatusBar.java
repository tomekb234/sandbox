package sandbox.gui;

import javafx.animation.AnimationTimer;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import sandbox.Sandbox;
import sandbox.utils.Controls;

public class StatusBar extends BorderPane {
    private Sandbox sandbox;
    private IntegerProperty entityCount;
    private IntegerProperty framesPerSecond;

    public StatusBar(Sandbox sandbox) {
        this.sandbox = sandbox;
        entityCount = new SimpleIntegerProperty();
        framesPerSecond = new SimpleIntegerProperty();

        setPadding(new Insets(5));
        setLeft(Controls.label("entity-count", entityCount));
        setRight(Controls.label("fps", framesPerSecond));
        new Timer().start();
    }

    private class Timer extends AnimationTimer {
        private static final long interval = 100_000_000; // 100ms
        private long next = 0;

        @Override
        public void handle(long time) {
            if (time < next)
                return;
            next = time + interval;
            entityCount.set(sandbox.physics.entityCount());
            framesPerSecond.set(sandbox.graphics.fps());
        }
    }
}
