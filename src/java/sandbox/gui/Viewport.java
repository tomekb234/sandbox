package sandbox.gui;

import javafx.animation.AnimationTimer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Cursor;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.robot.Robot;
import sandbox.engine.GraphicEngine;
import sandbox.utils.Utils;

import java.nio.ByteBuffer;
import java.util.Map;

// viewport for natively rendered graphics
public class Viewport extends Pane {
    private static final Map<KeyCode, Integer> keys = Map.of(
        KeyCode.W, GraphicEngine.keyForward,
        KeyCode.S, GraphicEngine.keyBackward,
        KeyCode.A, GraphicEngine.keyLeft,
        KeyCode.D, GraphicEngine.keyRight,
        KeyCode.SPACE, GraphicEngine.keyUp,
        KeyCode.SHIFT, GraphicEngine.keyDown
    );

    public final IntegerProperty fpsLimit;
    public final BooleanProperty ownWindow;
    public final BooleanProperty mouseCaptured;
    private GraphicEngine graphics;
    private MainScene scene;
    private ImageView image;
    private PixelBuffer<ByteBuffer> buffer;
    private double lastMouseX;
    private double lastMouseY;

    public Viewport(GraphicEngine graphics, MainScene scene) {
        this.graphics = graphics;
        this.scene = scene;
        image = new ImageView();
        fpsLimit = new SimpleIntegerProperty(60);
        ownWindow = new SimpleBooleanProperty(false);
        mouseCaptured = new SimpleBooleanProperty(false);

        getChildren().add(image);
        widthProperty().addListener(o -> resize());
        heightProperty().addListener(o -> resize());
        Utils.listen(fpsLimit, graphics::setFpsLimit);
        ownWindow.addListener(o -> resize());

        setOnMouseClicked(this::mouseClicked);
        setOnMouseMoved(this::mouseMoved);
        setOnKeyPressed(this::keyPressed);
        setOnKeyReleased(this::keyReleased);
        cursorProperty().bind(Bindings.createObjectBinding(this::cursor, ownWindow, mouseCaptured));

        new Timer().start();
    }

    private void resize() {
        var width = (int)getWidth();
        var height = (int)getHeight();
        if (width <= 0 || height <= 0)
            return;
        if (ownWindow.get()) {
            // own viewport window has slightly better performance
            graphics.newBuffer(width, height, true);
            image.setImage(null);
        } else {
            // using JavaFX's native rendering available since version 13
            var byteBuffer = graphics.newBuffer(width, height, false);
            var format = PixelFormat.getByteBgraPreInstance();
            buffer = new PixelBuffer<>(width, height, byteBuffer, format);
            image.setImage(new WritableImage(buffer));
        }
    }

    private class Timer extends AnimationTimer {
        @Override
        public void handle(long time) {
            if (ownWindow.get())
                return;
            if (graphics.nextFrameAvailable()) {
                buffer.updateBuffer(c -> null);
                graphics.nextFrameAcknowledge();
            }
        }
    }

    private void centerMouse() {
        var x = getWidth() / 2;
        var y = getHeight() / 2;
        new Robot().mouseMove(localToScreen(x, y));
        lastMouseX = x;
        lastMouseY = y;
    }

    private void mouseClicked(MouseEvent event) {
        if (ownWindow.get())
            return;

        var x = event.getX();
        var y = event.getY();
        var button = event.getButton();

        if (button == MouseButton.PRIMARY) {
            if (mouseCaptured.get()) {
                x = getWidth() / 2;
                y = getHeight() / 2;
            } var multiple = event.isShiftDown() || event.isControlDown();
            var pos = graphics.getCameraPosition();
            var dir = graphics.mouseDirection((int)x, (int)y);
            scene.select(multiple, pos, dir);
        }

        else if (button == MouseButton.SECONDARY) {
            if (!mouseCaptured.get()) {
                centerMouse();
                requestFocus();
                mouseCaptured.set(true);
            } else {
                for (var key : keys.values())
                    graphics.keyReleased(key);
                mouseCaptured.set(false);
            }
        }
    }

    private void mouseMoved(MouseEvent event) {
        if (!mouseCaptured.get())
            return;
        var x = event.getX();
        var y = event.getY();
        var dx = x - lastMouseX;
        var dy = y - lastMouseY;
        lastMouseX = x;
        lastMouseY = y;
        graphics.mouseMoved((int)dx, (int)dy);
        // centering the cursor after every move causes problems with calculating dx and dy
        // moving the mouse quickly enough can release the cursor from viewport
        var w = getWidth();
        var h = getHeight();
        if (x < w * 1/4 || x > w * 3/4 || y < h * 1/4 || y > h * 3/4)
            centerMouse();
    }

    private void keyPressed(KeyEvent event) {
        if (!mouseCaptured.get())
            return;
        var key = keys.get(event.getCode());
        if (key != null)
            graphics.keyPressed(key);
    }

    private void keyReleased(KeyEvent event) {
        if (!mouseCaptured.get())
            return;
        var key = keys.get(event.getCode());
        if (key != null)
            graphics.keyReleased(key);
    }

    private Cursor cursor() {
        if (ownWindow.get())
            return Cursor.DEFAULT;
        if (mouseCaptured.get())
            return Cursor.NONE;
        return Cursor.CROSSHAIR;
    }
}
