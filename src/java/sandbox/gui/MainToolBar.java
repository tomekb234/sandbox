package sandbox.gui;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import sandbox.Sandbox;
import sandbox.utils.Controls;
import sandbox.utils.Utils;

public class MainToolBar extends ToolBar {
    private ObservableList<Sandbox.ModelFactory> models;

    public MainToolBar(Sandbox sandbox, MainScene scene) {
        var resume = Controls.button("resume");
        getItems().add(resume);
        resume.disableProperty().bind(Bindings.not(sandbox.paused));
        resume.setOnAction(e -> sandbox.resume());

        var pause = Controls.button("pause");
        getItems().add(pause);
        pause.disableProperty().bind(sandbox.paused);
        pause.setOnAction(e -> sandbox.pause());

        var revert = Controls.button("revert");
        getItems().add(revert);
        revert.disableProperty().bind(Bindings.not(sandbox.revertible));
        revert.setOnAction(e -> sandbox.revert());

        var speed = new Slider(0, 1, 1);
        getItems().add(Controls.icon("speed"));
        getItems().add(speed);
        speed.valueProperty().bindBidirectional(sandbox.speed);

        var space = new HBox();
        getItems().add(space);
        HBox.setHgrow(space, Priority.ALWAYS);

        models = FXCollections.observableArrayList();
        Bindings.bindContent(models, sandbox.modelFactories);

        var modelChoiceBox = new ChoiceBox<Sandbox.ModelFactory>();
        var modelSelection = modelChoiceBox.getSelectionModel();
        getItems().add(modelChoiceBox);
        modelChoiceBox.visibleProperty().bind(Bindings.isNotEmpty(models));
        modelChoiceBox.setConverter(Utils.stringConverter(f -> f.model));

        models.addListener((Observable o) -> {
            modelChoiceBox.getItems().setAll(models);
            if (!models.isEmpty())
                modelSelection.selectLast();
        });

        var addModel = Controls.button("model");
        getItems().add(addModel);
        addModel.visibleProperty().bind(Bindings.isNotEmpty(models));
        addModel.setOnAction(e -> scene.selection.set(modelSelection.getSelectedItem().addNew()));

        for (var factory : sandbox.factories) {
            var addEntity = Controls.button(factory.type);
            getItems().add(addEntity);
            addEntity.setOnAction(e -> scene.selection.set(factory.addNew()));
        }
    }
}
