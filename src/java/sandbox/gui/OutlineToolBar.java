package sandbox.gui;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import sandbox.utils.Controls;

public class OutlineToolBar extends ToolBar {
    public OutlineToolBar(MainScene scene) {
        var allowRemove = Bindings.createBooleanBinding(scene::allowRemove, scene.multipleSelection);
        var allowGroup = Bindings.createBooleanBinding(scene::allowGroup, scene.multipleSelection);

        var removeButton = Controls.button("remove");
        getItems().add(removeButton);
        removeButton.disableProperty().bind(Bindings.not(allowRemove));
        removeButton.setOnAction(e -> scene.remove());

        var space = new HBox();
        getItems().add(space);
        HBox.setHgrow(space, Priority.ALWAYS);

        var groupButton = Controls.button("group-selected");
        getItems().add(groupButton);
        groupButton.disableProperty().bind(Bindings.not(allowGroup));
        groupButton.setOnAction(e -> scene.group());
    }
}
