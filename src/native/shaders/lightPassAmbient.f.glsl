#version 330 core

in vec2 texCoords;

uniform sampler2D texNormal;
uniform sampler2D texDiffuse;

uniform vec4 backgroundColor;

out vec4 FragColor;

void main(){
    vec3 wsNormal = texture(texNormal, texCoords).xyz;
    vec3 diffuseColor = texture(texDiffuse, texCoords).rgb;

    if(length(wsNormal)<0.5)
        FragColor = vec4(backgroundColor.rgb, 1.0);
    else
        FragColor = vec4(diffuseColor*(backgroundColor.rgb*backgroundColor.a*0.5+0.005), 1.0);
}
