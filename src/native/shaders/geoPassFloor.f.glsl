#version 330 core

in vec3 wsPosition;
in vec2 floorTexCoord;

uniform sampler2D floorTexture;

layout (location = 0) out vec4 gBufPosition;
layout (location = 1) out vec4 gBufNormal;
layout (location = 2) out vec4 gBufDiffuse;

void main(){
    gBufPosition = vec4(wsPosition, 1.0f);
    gBufNormal = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    gBufDiffuse = texture(floorTexture, floorTexCoord);
}