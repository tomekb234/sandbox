#version 330 core

in vec2 texCoords;

uniform sampler2D hdrFrame;
uniform float gamma;
uniform bool yflip;

out vec4 FragColor;

void main(){
    vec3 fragment = texture(hdrFrame, yflip?vec2(texCoords.x, 1.0-texCoords.y):texCoords).rgb;
    FragColor = vec4(pow(fragment, vec3(1.0/gamma)), 1.0);
}