#version 330 core

in vec2 texCoords;

uniform sampler2D texPosition;
uniform sampler2D texNormal;
uniform sampler2D texDiffuse;

uniform vec3 wsCameraPosition;
uniform vec3 lightPosDir;
uniform vec3 lightColor;
uniform bool lightModePoint;
uniform float lightBrightness;

out vec4 FragColor;

float sqr(float x){
    return x*x;
}

void main(){
    //initial values
    vec3 wsPosition = texture(texPosition, texCoords).xyz;
    vec3 wsNormal = texture(texNormal, texCoords).xyz;
    vec3 diffuseColor = texture(texDiffuse, texCoords).rgb;

    if(length(wsNormal)<0.5)
        discard;
    vec3 wsLightDir = lightModePoint ? normalize(wsPosition-lightPosDir) : lightPosDir;

    //diffuse component
    vec3 diffuseComponent = max(dot(wsNormal, -wsLightDir), 0.0f)*diffuseColor;

    //specular component
    vec3 wsReflectedDir = reflect(wsLightDir, wsNormal);
    vec3 wsViewDir = normalize(wsCameraPosition-wsPosition);
    vec3 specularComponent = vec3(pow(max(dot(wsReflectedDir, wsViewDir), 0.0), 32));

    //brightness and attenuation
    float powerFactor = (lightModePoint ? 1.0/sqr(length(wsPosition-lightPosDir)) : 1.0)*lightBrightness;

    FragColor = vec4((diffuseComponent+specularComponent)*lightColor*powerFactor, 1.0f);
}
