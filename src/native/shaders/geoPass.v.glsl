#version 330 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in vec3 vCol;
layout (location = 2) in vec3 vNorm;

uniform mat4 matrixM;
uniform mat4 matrixMVP;
uniform vec4 colorFilter;

out vec3 wsPosition;
out vec3 wsNormal;
out vec3 diffuseColor;

void main(){
    gl_Position = matrixMVP*vec4(vPos, 1.0);
    wsPosition = vec3(matrixM*vec4(vPos, 1.0));
    wsNormal = mat3(matrixM)*vNorm;
    diffuseColor = vec3(colorFilter)*colorFilter.a + vCol*(1.0-colorFilter.a);
}