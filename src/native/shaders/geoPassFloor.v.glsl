#version 330 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in vec2 vTexCoord;

uniform mat4 matrixVP;

out vec3 wsPosition;
out vec2 floorTexCoord;

void main(){
    gl_Position = matrixVP*vec4(vPos, 1.0);
    wsPosition = vPos;
    floorTexCoord = vTexCoord;
}