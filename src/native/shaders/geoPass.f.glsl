#version 330 core

in vec3 wsPosition;
in vec3 wsNormal;
in vec3 diffuseColor;

layout (location = 0) out vec4 gBufPosition;
layout (location = 1) out vec4 gBufNormal;
layout (location = 2) out vec4 gBufDiffuse;

void main(){
    gBufPosition = vec4(wsPosition, 1.0f);
    gBufNormal = vec4(normalize(wsNormal), 1.0f);
    gBufDiffuse = vec4(diffuseColor, 1.0f);
}