#ifndef SANDBOX_UTILS_H
#define SANDBOX_UTILS_H

#include <queue>
#include <mutex>
#include <atomic>
#include <functional>
#include <glm/glm.hpp>
#include <LinearMath/btVector3.h>
#include <LinearMath/btQuaternion.h>

namespace sb {
    class RequestQueue {
    private:
        std::queue<std::function<void()>> requests;
        std::mutex mut;
    public:
        void push(std::function<void()> request);
        bool handle();
    };

    class ReferenceCountingClass{
    private:
        std::atomic_int linkCounter;
    public:
        ReferenceCountingClass();
        virtual ~ReferenceCountingClass() = default;

        void link();
        void unlink();
        bool isLastLink();
    };

    extern float nan;
    glm::vec3 Vec(btVector3);
    btVector3 Vec(glm::vec3);

    glm::vec3 Euler(const btQuaternion& qua);
    btQuaternion Euler(const glm::vec3& vec);
}

#endif
