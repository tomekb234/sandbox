#ifndef SANDBOX_SHAREDBUFFER_H
#define SANDBOX_SHAREDBUFFER_H

#include <mutex>
#include <atomic>
#include <condition_variable>
#include <RenderTarget.h>

namespace sb{
    class SharedBuffer : public RenderTarget{
    private:
        const int width, height;
        unsigned char *frontBuffer;
        unsigned int FBO, RBOcolor;

        bool backReady;
        std::atomic_bool frontReady;
        std::mutex bufferFlagMutex;
        std::condition_variable bufferFlagCond;

        void moveToFrontBuffer();
    public:
        SharedBuffer(int width, int height);
        ~SharedBuffer();

        void initOpenGLPart();
        void deinitOpenGLPart();
        bool waitBeforeRendering();
        void preRender();
        void postRender();

        int getWidth()const;
        int getHeight()const;
        bool needsYFlip()const;

        unsigned char* getFrontBuffer();
        bool nextFrameAvailable();
        void nextFrameAcknowledge();
    };
}

#endif
