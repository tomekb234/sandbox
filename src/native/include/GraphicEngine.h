#ifndef SANDBOX_GRAPHICENGINE_H
#define SANDBOX_GRAPHICENGINE_H

#include <thread>
#include <mutex>
#include <utility>
#include <condition_variable>
#include <list>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <Utils.h>
#include <Shaders.h>
#include <RenderTarget.h>
#include <Camera.h>
#include <Mesh.h>

namespace sb{
    class Entity;
    class DirectionalLight;
    class PointLight;

    class GraphicEngine{
    public:
        //TABS_NUM must be the last entry
        enum Key {KEY_FORWARD=0, KEY_BACKWARD, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEYS_NUM};

    private:
        std::thread graphicThread;
        std::mutex initMutex;
        std::condition_variable initCond;
        enum{OFF, ON, CHANGING} state;

        GLFWwindow *window;
        RenderTarget *target;
        Camera *camera;

        RequestQueue requestQueue;

        unsigned int gBuffer, midFramebuffer;
        unsigned int posTextureBuffer, normTextureBuffer, diffTextureBuffer, depthBuffer, midBuffer;
        unsigned int quadVBO, quadVAO;

        glm::vec4 backgroundColor;
        std::list<Entity*> entities;
        std::list<DirectionalLight*> directionalLights;
        std::list<PointLight*> pointLights;

        unsigned int floorVBO, floorVAO, floorTEX;

        bool keyboardState[KEYS_NUM];

        GeoPassShader *geoPassShader;
        GeoPassFloorShader *geoPassFloorShader;
        LightPassShader *lightPassShader;
        LightPassAmbientShader *lightPassAmbientShader;
        SingleColorShader *singleColorShader;
        PostprocessShader *postprocessShader;

        float lastFps=0, fpsLimit=60;

        bool init();
        void deinit();
        void mainLoop();

        void drawEntity(Entity *ent, const glm::mat4& cameraTransform, bool primitiveShader=false);
        void moveCamera(Camera *cam, float factor);
        void remakeBuffers(int width, int height);

    public:
        GraphicEngine();
        ~GraphicEngine();

        bool start();
        void stop();

        void enqueueRequest(std::function<void()> request);
        Camera*& getCamera();
        glm::vec3 mouseDirection(int screenX, int screenY);
        void setKeyState(Key key, bool keyState);
        void clearKeyboardState();
        float getFps();

        // functions called from request queue
        void changeTarget(RenderTarget* newTarget);
        void registerMesh(Mesh* mesh);
        void addEntity(Entity* entity);
        void removeEntity(Entity* entity);
        void addDirectionalLight(DirectionalLight* dlight);
        void removeDirectionalLight(DirectionalLight* dlight);
        void addPointLight(PointLight* plight);
        void removePointLight(PointLight* plight);
        void changeBackground(glm::vec4 color);
        void setGamma(float gamma);
        void setFpsLimit(float limit);
    };
}

#endif
