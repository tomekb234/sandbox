#ifndef SANDBOX_PHYSICSENGINE_H
#define SANDBOX_PHYSICSENGINE_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <list>
#include <btBulletDynamicsCommon.h>
#include <Utils.h>

namespace sb{
    class Entity;
    class Rope;

    class PhysicsEngine{
        std::thread physicsThread;
        std::mutex initMutex;
        std::condition_variable initCond;
        enum{OFF, ON, CHANGING} state;

        btDefaultCollisionConfiguration *collisionConfig;
        btCollisionDispatcher *collisionDispatcher;
        btBroadphaseInterface *broadphaseInterface;
        btSequentialImpulseConstraintSolver *constraintSolver;
        btDiscreteDynamicsWorld *world;

        RequestQueue requestQueue;

        std::list<Entity*> entities;
        std::list<Rope*> ropes;
        btStaticPlaneShape *floorShape;
        btRigidBody *floorBody;

        bool simulatonActive;
        float timeFactor;
        btVector3 gravity;

        void mainLoop();

    public:
        PhysicsEngine();
        ~PhysicsEngine();

        bool start();
        void stop();

        void enqueueRequest(std::function<void()> request);
        btVector3 getGravity();
        int rayCast(btVector3 pos, btVector3 dir);
        int entityCount();

        // functions called from request queue
        void addEntity(Entity* entity);
        void removeEntity(Entity* entity);
        void readdEntity(Entity* entity);
        void addRope(Rope* rope);
        void removeRope(Rope* rope);
        void setGravity(btVector3 newGravity);
        void playPause(bool play);
        void setTimeFactor(float factor);
    };
}

#endif
