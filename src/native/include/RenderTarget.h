#ifndef SANDBOX_RENDERTARGET_H
#define SANDBOX_RENDERTARGET_H

#include <atomic>
#include <Utils.h>

namespace sb{
    class RenderTarget : public ReferenceCountingClass{
    public:
        RenderTarget();
        virtual ~RenderTarget();

        virtual void initOpenGLPart()=0;
        virtual void deinitOpenGLPart()=0;
        virtual bool waitBeforeRendering()=0;
        virtual void preRender()=0;
        virtual void postRender()=0;

        virtual int getWidth()const=0;
        virtual int getHeight()const=0;
        virtual bool needsYFlip()const=0;
    };

    class NullTarget : public RenderTarget{
    public:
        NullTarget();
        ~NullTarget();

        void initOpenGLPart();
        void deinitOpenGLPart();
        bool waitBeforeRendering();
        void preRender();
        void postRender();

        int getWidth()const;
        int getHeight()const;
        bool needsYFlip()const;
    };
}

#endif
