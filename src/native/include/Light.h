#ifndef SANDBOX_LIGHT_H
#define SANDBOX_LIGHT_H

#include <list>
#include <glm/glm.hpp>
#include <Utils.h>

namespace sb{
    struct Light : public ReferenceCountingClass{
        glm::vec3 color = glm::vec3(1.0);
        float brightness = 1.0;
    };

    struct DirectionalLight : public Light{
        glm::vec3 direction = glm::vec3(0.0, -1.0, 0.0);
        std::list<DirectionalLight*>::iterator graphicEngineIterator;
    };

    struct PointLight : public Light{
        glm::vec3 position = glm::vec3(0.0);
        std::list<PointLight*>::iterator graphicEngineIterator;
    };
};

#endif
