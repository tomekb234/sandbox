#ifndef SANDBOX_CAMERA_H
#define SANDBOX_CAMERA_H

#include <glm/glm.hpp>

namespace sb{
    class Camera{
    private:
        glm::vec3 position,direction;
        float pitch, yaw;

        float ratio, fov;

        glm::mat4 viewMatrix, projectionMatrix, combinedMatrix;

        void updateViewMatrix();
        void updateProjectionMatrix();
    public:
        Camera();
        Camera(glm::vec3 pos, glm::vec3 viewRay);
        ~Camera();

        void setRatio(float r);
        void setFOV(float f);

        void rotate(float xmov, float ymov);
        void step(float forward, float right, float up);

        glm::mat4 getTransform();
        glm::vec3 getPosition();
        glm::vec3 getDirection();
    };
}

#endif
