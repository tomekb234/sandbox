#ifndef SANDBOX_OWNWINDOW_H
#define SANDBOX_OWNWINDOW_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <RenderTarget.h>
#include <GraphicEngine.h>

namespace sb{
    class OwnWindow : public RenderTarget{
    private:
        const int width, height;

        bool controlCaptured;
        GLFWwindow *window;
        GraphicEngine* engine;

        static void keyboardCallback(GLFWwindow*,int,int,int,int);
        static void mouseCallback(GLFWwindow*,double,double);
        static void mouseButtonCallback(GLFWwindow*,int,int,int);

        double lastCursorPosX, lastCursorPosY;
    public:
        OwnWindow(int w, int h, GraphicEngine* eng);
        ~OwnWindow();

        void initOpenGLPart();
        void deinitOpenGLPart();
        bool waitBeforeRendering();
        void preRender();
        void postRender();

        int getWidth()const;
        int getHeight()const;
        bool needsYFlip()const;
    };
}

#endif
