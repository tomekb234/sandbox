#ifndef SANDBOX_ROPE_H
#define SANDBOX_ROPE_H

#include <btBulletDynamicsCommon.h>
#include <PhysicsEngine.h>
#include <Utils.h>

namespace sb {
    class Rope : public ReferenceCountingClass {
        friend class PhysicsEngine;

        float length = 5.0f;
        int entityIdA;
        int entityIdB;

        btRigidBody* bodyA;
        btRigidBody* bodyB;
        btRigidBody* endA;
        btRigidBody* endB;
        btMotionState* msA;
        btMotionState* msB;
        btPoint2PointConstraint* constrA;
        btPoint2PointConstraint* constrB;
        btSliderConstraint* constr;

        std::mutex physicsLinkMutex;
        PhysicsEngine* physicsEngine = nullptr;
        std::list<Rope*>::iterator physicsEngineIterator;

        // functions called from physics engine thread
        void initPhysicsPart();
        void deinitPhysicsPart();
        void updateTransform();
        void updateLength();

        public:

        Rope(int entityIdA, int entityIdB);

        // called from main thread
        void setLength(float length);
    };
};

#endif
