#ifndef SANDBOX_MESH_H
#define SANDBOX_MESH_H

#include <btBulletDynamicsCommon.h>

namespace sb{
    class Mesh{
    public:
        //TABS_NUM must be the last entry
        enum Tabs {VERTEX_TAB=0, COLOR_TAB, NORMAL_TAB, TABS_NUM};
        const static int TabSizes[Tabs::TABS_NUM];
        enum ShapeType {CUBOID=0, SPHERE, CONE, CYLINDER, CONVEX_HULL};

    private:
        int size;
        float* tab[Tabs::TABS_NUM];
        ShapeType shapeType;
        btVector3 shapeParams;

        unsigned int VBO[Tabs::TABS_NUM];
        btConvexShape *shape;

    public:
        Mesh(int s, ShapeType st=CONVEX_HULL, btVector3 sp=btVector3());
        ~Mesh();

        void setTab(Tabs tabId, const float *data);
        void setShapeType();
        int getSize();

        void initOpenGLPart();
        void deinitOpenGLPart();
        void bindVBO(Tabs tabId);

        void initBulletPart();
        void deinitBulletPart();
        btConvexShape* getShape();
    };
}

#endif
