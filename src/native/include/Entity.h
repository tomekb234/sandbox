#ifndef SANDBOX_ENTITY_H
#define SANDBOX_ENTITY_H

#include <list>
#include <mutex>
#include <glm/glm.hpp>
#include <btBulletDynamicsCommon.h>
#include <GraphicEngine.h>
#include <PhysicsEngine.h>
#include <Mesh.h>
#include <Utils.h>

namespace sb{
    class MotionState : public btMotionState{
        private:
            glm::mat4 *transform;
        public:
            MotionState(glm::mat4 *t);
            void getWorldTransform(btTransform &comTransform) const override;
            void setWorldTransform(const btTransform &comTransform) override;
    };

    class Entity : public ReferenceCountingClass{
        friend GraphicEngine;
        friend PhysicsEngine;

        //entity parameters
        int id;
        glm::mat4 transform, transformCopy;
        Mesh *mesh;
        glm::vec3 positionToSet = glm::vec3(0.0);
        glm::vec3 angularPositionToSet = glm::vec3(0.0);
        glm::vec3 velocityToSet = glm::vec3(0.0);
        glm::vec3 angularVelocityToSet = glm::vec3(0.0);
        glm::vec3 acceleration = glm::vec3(0.0);
        glm::vec4 color = glm::vec4(0.0);

        float mass = 1.0;
        bool immovable = false;
        float friction = 0.5;
        float restitution = 0.0;
        float size = 1.0;

        //graphic data
        GraphicEngine *graphicsEngine;
        unsigned int VAO;
        bool selected = false;
        std::list<Entity*>::iterator graphicEngineIterator;

        //physics data
        PhysicsEngine *physicsEngine;
        std::list<Entity*>::iterator physicsEngineIterator;
        enum{OFF, LINKING, ON, UNLINKING} physicsLinkState = OFF;
        std::mutex physicsLinkMutex;
        btRigidBody *rigidBody;
        MotionState *motionState;
        btCollisionShape* ownShape = nullptr;

        //functions called from graphics engine thread
        void initGraphicsPart(GraphicEngine *engine);
        void deinitGraphicsPart();

        //functions called from physics engine thread
        void initPhysicsPart(PhysicsEngine *engine);
        void dumpDataFromPhysics();
        void deinitPhysicsPart();

        void updatePosition();
        void updateAngularPosition();
        void updateVelocity();
        void updateAngularVelocity();
        void updateInertia();
        void updateFriction();
        void updateRestitution();
        void updateAcceleration();
        void updateSize();

    public:
        Entity();
        ~Entity();

        //functions called from main thread
        void initMesh(Mesh* m);
        int getId();
        glm::vec3 getPosition();
        void setPosition(glm::vec3 newPos);
        glm::vec3 getAngularPosition();
        void setAngularPosition(glm::vec3 newAngPos);
        glm::vec3 getVelocity();
        void setVelocity(glm::vec3 newVel);
        glm::vec3 getAngularVelocity();
        void setAngularVelocity(glm::vec3 newAngVel);
        void setColor(glm::vec4 newColor);
        void setMass(float newMass);
        void setImmovable(bool newState);
        void setFriction(float newFriction);
        void setRestitution(float newRestitution);
        void setAcceleration(glm::vec3 newAcceleration);
        void setSize(float size);
        void setSelected(bool newSelected);

        void setTempPhysicsState(bool link);
    };
}

#endif
