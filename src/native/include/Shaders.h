#ifndef SANDBOX_SHADERS_H
#define SANDBOX_SHADERS_H

#include <string>
#include <glad/glad.h>
#include <glm/glm.hpp>

namespace sb{
    class Shader{
    protected:
        uint programId;

        static uint loadShaderCode(std::string code, GLenum type);
    public:
        Shader(std::string vCode, std::string fCode);
        ~Shader();

        void activate();
    };

    class GeoPassShader : public Shader{
    private:
        int matrixMUniformId, matrixMVPUniformId;
        int colorFilterUniformId;

    public:
        GeoPassShader();

        void setMatrixMVP(const glm::mat4 &matrix);
        void setMatrixM(const glm::mat4 &matrix);
        void setColorFilter(const glm::vec4 &vec);
    };

    class GeoPassFloorShader : public Shader{
    private:
        int matrixVPUniformId;

    public:
        GeoPassFloorShader();

        void setMatrixVP(const glm::mat4 &matrix);
    };

    class LightPassShader : public Shader{
    private:
        int cameraPosUniformId;
        int lightPosDirUniformId, lightColorUniformId, lightTypeUniformId, lightBrightnessUniformId;

    public:
        LightPassShader();

        void setCameraPos(const glm::vec3 &vec);
        void setLightPosDir(const glm::vec3 &vec);
        void setLightColor(const glm::vec3 &vec);
        void setLightMode(bool pointMode);
        void setLightBrightness(float brightness);
    };

    class LightPassAmbientShader : public Shader{
    private:
        int backgroundColorUniformId;

    public:
        LightPassAmbientShader();

        void setBackgroundColor(const glm::vec4 &vec);
    };

    class SingleColorShader : public Shader{
    private:
        int matrixMVPUniformId, colorUniformId;

    public:
        SingleColorShader();

        void setMatrixMVP(const glm::mat4 &matrix);
        void setColor(const glm::vec4 &color);
    };

    class PostprocessShader : public Shader{
    private:
        int gammaUniformId, yFlipUniformId;

    public:
        PostprocessShader();

        void setGamma(float gamma);
        void setYFlip(bool flip);
    };
}

#endif
