#include <RenderTarget.h>

#include <thread>

sb::RenderTarget::RenderTarget(){ }

sb::RenderTarget::~RenderTarget(){ } 

sb::NullTarget::NullTarget(){ }

sb::NullTarget::~NullTarget(){ }

void sb::NullTarget::initOpenGLPart(){ }

void sb::NullTarget::deinitOpenGLPart(){ }

bool sb::NullTarget::waitBeforeRendering(){
    const int timeout = 50;
    std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
    return false;
}

void sb::NullTarget::preRender(){ }

void sb::NullTarget::postRender(){ }

int sb::NullTarget::getWidth()const{
    return 1;
}

int sb::NullTarget::getHeight()const{
    return 1;
}

bool sb::NullTarget::needsYFlip()const{
    return false;
}
