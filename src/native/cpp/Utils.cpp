#include <Utils.h>

void sb::RequestQueue::push(std::function<void()> request) {
    std::lock_guard<std::mutex> lock(mut);
    requests.push(request);
}

bool sb::RequestQueue::handle() {
    std::lock_guard<std::mutex> lock(mut);
    if (requests.empty())
        return false;
    std::function<void()> request = requests.front();
    requests.pop();
    request();
    return true;
}

sb::ReferenceCountingClass::ReferenceCountingClass() : linkCounter(1){}

void sb::ReferenceCountingClass::link(){
    linkCounter++;
}

void sb::ReferenceCountingClass::unlink(){
    if(--linkCounter==0)
        delete this;
}

bool sb::ReferenceCountingClass::isLastLink(){
    return linkCounter==1;
}

float sb::nan = std::numeric_limits<float>::quiet_NaN();

glm::vec3 sb::Vec(btVector3 v){
    return glm::vec3(v.x(), v.y(), v.z());
}

btVector3 sb::Vec(glm::vec3 v){
    return btVector3(v.x, v.y, v.z);
}

glm::vec3 sb::Euler(const btQuaternion& qua){
    glm::vec3 vec;
    qua.getEulerZYX(vec.z, vec.y, vec.x);
    return vec;
}

btQuaternion sb::Euler(const glm::vec3& vec){
    btQuaternion qua;
    qua.setEulerZYX(vec.z, vec.y, vec.x);
    return qua;
}
