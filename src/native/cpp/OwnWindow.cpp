#include <OwnWindow.h>

sb::OwnWindow::OwnWindow(int w, int h, GraphicEngine* eng) : width(w),height(h),controlCaptured(false),engine(eng){ }

sb::OwnWindow::~OwnWindow(){ }

void sb::OwnWindow::keyboardCallback(GLFWwindow *wnd, int key, int, int action, int){
    OwnWindow *This = (OwnWindow*)glfwGetWindowUserPointer(wnd);

    if(This->controlCaptured){
        if(key==GLFW_KEY_W)
            This->engine->setKeyState(GraphicEngine::KEY_FORWARD, action!=GLFW_RELEASE);
        else if(key==GLFW_KEY_S)
            This->engine->setKeyState(GraphicEngine::KEY_BACKWARD, action!=GLFW_RELEASE);
        else if(key==GLFW_KEY_A)
            This->engine->setKeyState(GraphicEngine::KEY_LEFT, action!=GLFW_RELEASE);
        else if(key==GLFW_KEY_D)
            This->engine->setKeyState(GraphicEngine::KEY_RIGHT, action!=GLFW_RELEASE);
        else if(key==GLFW_KEY_SPACE)
            This->engine->setKeyState(GraphicEngine::KEY_UP, action!=GLFW_RELEASE);
        else if(key==GLFW_KEY_LEFT_SHIFT)
            This->engine->setKeyState(GraphicEngine::KEY_DOWN, action!=GLFW_RELEASE);
    }
}

void sb::OwnWindow::mouseCallback(GLFWwindow* wnd, double xpos, double ypos){
    OwnWindow *This = (OwnWindow*)glfwGetWindowUserPointer(wnd);

    This->engine->getCamera()->rotate(int(xpos-This->lastCursorPosX), -int(ypos-This->lastCursorPosY));

    This->lastCursorPosX = xpos;
    This->lastCursorPosY = ypos;
}

void sb::OwnWindow::mouseButtonCallback(GLFWwindow* wnd, int button, int action, int) {
    OwnWindow *This = (OwnWindow*)glfwGetWindowUserPointer(wnd);

    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
        if(This->controlCaptured){
            glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfwSetCursorPosCallback(wnd, NULL);
            This->engine->clearKeyboardState();
        }
        else{
            glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfwGetCursorPos(wnd, &This->lastCursorPosX,&This->lastCursorPosY);
            glfwSetCursorPosCallback(wnd, mouseCallback);
        }
        This->controlCaptured = !This->controlCaptured;
    }
}

void sb::OwnWindow::initOpenGLPart(){
    window = glfwGetCurrentContext();
    glfwSetWindowUserPointer(window, this);
    glfwShowWindow(window);
    glfwSetWindowSize(window, width, height);

    glfwSetKeyCallback(window, keyboardCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
}

void sb::OwnWindow::deinitOpenGLPart(){
    glfwSetKeyCallback(window, NULL);
    glfwSetMouseButtonCallback(window, NULL);
    glfwSetCursorPosCallback(window, NULL);

    glfwSetWindowSize(window, 1, 1);
    glfwHideWindow(window);
}

bool sb::OwnWindow::waitBeforeRendering(){
    return true;
}

void sb::OwnWindow::preRender(){
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0,0,width, height);
}

void sb::OwnWindow::postRender(){
    glfwSwapBuffers(window);
}

int sb::OwnWindow::getWidth()const{
    return width;
}

int sb::OwnWindow::getHeight()const{
    return height;
}

bool sb::OwnWindow::needsYFlip()const{
    return false;
}
