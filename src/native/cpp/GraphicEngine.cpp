#include <GraphicEngine.h>

#include <chrono>
#include <Entity.h>
#include <Light.h>

sb::GraphicEngine::GraphicEngine():state(OFF){ }

sb::GraphicEngine::~GraphicEngine(){ }

bool sb::GraphicEngine::init(){
    if(!glfwInit())
        return false;

    //create dummy window (witch OpenGL context)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    window = glfwCreateWindow(1, 1, "Sandbox viewport", NULL, NULL);
    if(!window){
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window);

    //link OpenGL functions
    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        glfwTerminate();
        return false;
    }

    //load shaders
    try{
        geoPassShader = new GeoPassShader();
        geoPassFloorShader = new GeoPassFloorShader();
        lightPassShader = new LightPassShader();
        lightPassAmbientShader = new LightPassAmbientShader();
        singleColorShader = new SingleColorShader();
        postprocessShader = new PostprocessShader();
    }catch(...){
        delete postprocessShader;
        delete singleColorShader;
        delete lightPassAmbientShader;
        delete lightPassShader;
        delete geoPassFloorShader;
        delete geoPassShader;
        glfwTerminate();
        return false;
    }

    //mics inits
    clearKeyboardState();
    camera = new Camera(glm::vec3(0.0,3.0,0.0), glm::vec3(0.0,0.0,-1.0));
    target = new NullTarget();
    remakeBuffers(1, 1);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glPolygonOffset(0.0, -1.0);

    //create screen-size quad
    float quadMesh[] = {-1.0f, -1.0f,
                         1.0f, -1.0f,
                         1.0f,  1.0f,
                        -1.0f,  1.0f};
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadMesh), quadMesh, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    //create floor
    unsigned char floorColorGrid[] = {100,100,100,255,   200,200,200,255,   200,200,200,255,   100,100,100,255};
    glGenTextures(1, &floorTEX);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, floorTEX);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, floorColorGrid);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    float floorMesh[] = {-3000.0f,  0.0f, 3000.0f , 0.0, 3000.0,
                         3000.0f,  0.0f, 3000.0f, 3000.0, 3000.0,
                         3000.0f,  0.0f, -3000.0f, 3000.0, 0.0,
                         -3000.0f, 0.0f, -3000.0f, 0.0, 0.0};
    glGenVertexArrays(1, &floorVAO);
    glGenBuffers(1, &floorVBO);
    glBindVertexArray(floorVAO);
    glBindBuffer(GL_ARRAY_BUFFER, floorVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(floorMesh), floorMesh, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*5, (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float)*5, (void*)(sizeof(float)*3));
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);

    return true;
}

void sb::GraphicEngine::deinit(){
    delete postprocessShader;
    delete lightPassAmbientShader;
    delete lightPassShader;
    delete geoPassShader;
    target->unlink();
    delete camera;
    glfwTerminate();
}

void sb::GraphicEngine::mainLoop(){
    //init engine
    bool result = init();
    {
        std::lock_guard<std::mutex> lock(initMutex);
        state = result?ON:OFF;
    }
    initCond.notify_one();
    if(!result)
        return;


    //main loop
    int frameCount = 0;
    auto timeOfLastFPSCount = std::chrono::high_resolution_clock::now();
    auto timeOfLastFrame = std::chrono::high_resolution_clock::now();
    while(true){
        //process request queue
        while(requestQueue.handle()){}
        if(state==CHANGING)
            break;

        //wait for target
        if(!target->waitBeforeRendering())
            continue;

        //throttle fps
        auto currentTime = std::chrono::high_resolution_clock::now();
        int msToWait = 1000.0/fpsLimit - std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-timeOfLastFrame).count();
        if(msToWait>30){
            std::this_thread::sleep_for(std::chrono::milliseconds(20));
            continue;
        }
        else if(msToWait>0)
            std::this_thread::sleep_for(std::chrono::milliseconds(msToWait));
        
        //move camera and start new frame time measurement
        auto deltaTime = std::chrono::high_resolution_clock().now()-timeOfLastFrame;
        moveCamera(camera, std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count());
        glm::mat4 cameraTransform = camera->getTransform();
        timeOfLastFrame = std::chrono::high_resolution_clock::now();

        //render stage I - Geometry Pass
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            //normal objects
        glEnable(GL_CULL_FACE);
        geoPassShader->activate();
        for(Entity* ent : entities){
            ent->transformCopy = ent->transform;
            drawEntity(ent, cameraTransform);
        }
        
            //floor
        glDisable(GL_CULL_FACE);
        geoPassFloorShader->activate();
        geoPassFloorShader->setMatrixVP(cameraTransform);
        glBindVertexArray(floorVAO);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);

        //render stage II - Lighting Pass
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glBindFramebuffer(GL_FRAMEBUFFER, midFramebuffer);
        glBindVertexArray(quadVAO);

            //ambient light and background
        glClear(GL_COLOR_BUFFER_BIT);
        lightPassAmbientShader->activate();
        lightPassAmbientShader->setBackgroundColor(backgroundColor);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

            //directional lights
        lightPassShader->activate();
        lightPassShader->setCameraPos(camera->getPosition());
        lightPassShader->setLightMode(false);
        for(DirectionalLight *dirLight : directionalLights){
            lightPassShader->setLightPosDir(dirLight->direction);
            lightPassShader->setLightColor(dirLight->color);
            lightPassShader->setLightBrightness(dirLight->brightness);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        }

            //point lights
        lightPassShader->setLightMode(true);
        for(PointLight *pointLight : pointLights){
            lightPassShader->setLightPosDir(pointLight->position);
            lightPassShader->setLightColor(pointLight->color);
            lightPassShader->setLightBrightness(pointLight->brightness);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        }

        //render stage II.V - Selected Objects
        singleColorShader->activate();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);
        float selectedAlpha = (sin(glfwGetTime() * M_PI) + 1) / 2;

            //blue blink
        singleColorShader->setColor(glm::vec4(0, 0.25, 1.0, selectedAlpha));
        for(Entity* ent : entities)
            if(ent->selected)
                drawEntity(ent, cameraTransform, true);

            //green blink
        glDepthFunc(GL_GREATER);
        singleColorShader->setColor(glm::vec4(0, 1.0, 0.25, selectedAlpha));
        for(Entity* ent : entities)
            if(ent->selected)
                drawEntity(ent, cameraTransform, true);
        glDepthFunc(GL_LESS);

        glDisable(GL_POLYGON_OFFSET_FILL);
        glDisable(GL_CULL_FACE);

        //render stage III - Postprocessing Pass - final rendering
        target->preRender();
        postprocessShader->activate();
        postprocessShader->setYFlip(target->needsYFlip());
        glBindVertexArray(quadVAO);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        glBindVertexArray(0);

        //post-render procedures
        target->postRender();
        glfwPollEvents();

        //count fps
        frameCount++;
        auto timeNow = std::chrono::high_resolution_clock::now();
        int msSinceLastFpsCount = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow-timeOfLastFPSCount).count();
        if(msSinceLastFpsCount>=1000){
            lastFps = 1000.0*frameCount/msSinceLastFpsCount;
            frameCount = 0;
            timeOfLastFPSCount = timeNow;
        }
    }


    //deinit engine
    deinit();
    state = OFF;
}

void sb::GraphicEngine::drawEntity(Entity *ent, const glm::mat4& cameraTransform, bool primitiveShader){
    glBindVertexArray(ent->VAO);
    glm::mat4 transform = ent->transformCopy;
    transform[0] *= ent->size;
    transform[1] *= ent->size;
    transform[2] *= ent->size;
    if(primitiveShader)
        singleColorShader->setMatrixMVP(cameraTransform*transform);
    else{
        geoPassShader->setMatrixM(transform);
        geoPassShader->setMatrixMVP(cameraTransform*transform);
        geoPassShader->setColorFilter(ent->color);
    }
    glDrawArrays(GL_TRIANGLES, 0, ent->mesh->getSize());
    glBindVertexArray(0);
}

void sb::GraphicEngine::moveCamera(Camera *cam, float factor){
    int fbAxis = int(keyboardState[KEY_FORWARD]) - int(keyboardState[KEY_BACKWARD]);
    int lrAxis = int(keyboardState[KEY_RIGHT]) - int(keyboardState[KEY_LEFT]);
    int udAxis = int(keyboardState[KEY_UP]) - int(keyboardState[KEY_DOWN]);

    if(fbAxis || lrAxis || udAxis)
        cam->step(factor*fbAxis, factor*lrAxis, factor*udAxis);
}

void sb::GraphicEngine::remakeBuffers(int width, int height){
    //clean old buffers
    glDeleteTextures(1, &posTextureBuffer);
    glDeleteTextures(1, &normTextureBuffer);
    glDeleteTextures(1, &diffTextureBuffer);
    glDeleteRenderbuffers(1, &depthBuffer);
    glDeleteFramebuffers(1, &gBuffer);
    glDeleteTextures(1, &midBuffer);
    glDeleteFramebuffers(1, &midFramebuffer);

    //G-buffer
    glGenFramebuffers(1, &gBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

    //position vector buffer
    glGenTextures(1, &posTextureBuffer);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, posTextureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, posTextureBuffer, 0);

    //normal vector buffer
    glGenTextures(1, &normTextureBuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, normTextureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normTextureBuffer, 0);

    //diffuse color buffer
    glGenTextures(1, &diffTextureBuffer);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, diffTextureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, diffTextureBuffer, 0);

    //depth buffer
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

    //finalize G-buffer
    unsigned int attachments[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, attachments);

    //mid-target framebuffer
    glGenFramebuffers(1, &midFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, midFramebuffer);

    glGenTextures(1, &midBuffer);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, midBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, midBuffer, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    unsigned int attachments2[] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, attachments2);
}

bool sb::GraphicEngine::start(){
    if(state!=OFF)
        return false;
    
    //start graphic thread
    state = CHANGING;
    this->graphicThread = std::thread(&sb::GraphicEngine::mainLoop, this);

    //wait for init completion
    std::unique_lock<std::mutex> lock(initMutex);
    initCond.wait(lock, [this]{return state!=CHANGING;});
    return state==ON;
}

void sb::GraphicEngine::stop(){
    if(state!=ON)
        return;
    
    state = CHANGING;
    graphicThread.join();
}

void sb::GraphicEngine::enqueueRequest(std::function<void()> request){
    requestQueue.push(request);
}

sb::Camera*& sb::GraphicEngine::getCamera(){
    return camera;
}

glm::vec3 sb::GraphicEngine::mouseDirection(int screenX, int screenY) {
    float x = 2 * ((float)screenX / target->getWidth()) - 1;
    float y = -2 * ((float)screenY / target->getHeight()) + 1;
    glm::mat4 inv = glm::inverse(camera->getTransform());
    glm::vec4 near = inv * glm::vec4(x, y, -1, 1);
    glm::vec4 far = inv * glm::vec4(x, y, 1, 1);
    near /= near.w;
    far /= far.w;
    return glm::normalize(glm::vec3(far - near));
}

void sb::GraphicEngine::setKeyState(Key key, bool keyState){
    keyboardState[key]=keyState;
}

void sb::GraphicEngine::clearKeyboardState() {
    for(int i=0;i<KEYS_NUM;i++)
        keyboardState[i]=false;
}

float sb::GraphicEngine::getFps() {
    return lastFps;
}

void sb::GraphicEngine::changeTarget(RenderTarget* newTarget){
    if (newTarget->isLastLink())
        newTarget->unlink();
    else {
        target->deinitOpenGLPart();
        target->unlink();
        newTarget->initOpenGLPart();
        camera->setRatio((float)newTarget->getWidth()/newTarget->getHeight());
        target = newTarget;
        remakeBuffers(newTarget->getWidth(), newTarget->getHeight());
    }
}

void sb::GraphicEngine::registerMesh(Mesh* mesh) {
    mesh->initOpenGLPart();
}

void sb::GraphicEngine::addEntity(Entity* entity) {
    entity->initGraphicsPart(this);
    entities.push_back(entity);
    entity->graphicEngineIterator = std::prev(entities.end());
}

void sb::GraphicEngine::removeEntity(Entity* entity) {
    entities.erase(entity->graphicEngineIterator);
    entity->deinitGraphicsPart();
}

void sb::GraphicEngine::addDirectionalLight(DirectionalLight* dlight) {
    directionalLights.push_back(dlight);
    dlight->graphicEngineIterator = std::prev(directionalLights.end());
}

void sb::GraphicEngine::removeDirectionalLight(DirectionalLight* dlight){
    directionalLights.erase(dlight->graphicEngineIterator);
}

void sb::GraphicEngine::addPointLight(PointLight* plight) {
    pointLights.push_back(plight);
    plight->graphicEngineIterator = std::prev(pointLights.end());
}

void sb::GraphicEngine::removePointLight(PointLight* plight) {
    pointLights.erase(plight->graphicEngineIterator);
}

void sb::GraphicEngine::changeBackground(glm::vec4 color) {
    if(!isnan(color.r)){
        backgroundColor.r = color.r;
        backgroundColor.g = color.g;
        backgroundColor.b = color.b;
    }
    if(!isnan(color.a))
        backgroundColor.a = color.a;
}

void sb::GraphicEngine::setGamma(float gamma){
    postprocessShader->activate();
    postprocessShader->setGamma(gamma);
}

void sb::GraphicEngine::setFpsLimit(float limit) {
    fpsLimit = limit;
}
