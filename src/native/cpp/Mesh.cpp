#include <Mesh.h>

#include <algorithm>
#include <glad/glad.h>

const int sb::Mesh::TabSizes[Tabs::TABS_NUM] = {3,3,3};

sb::Mesh::Mesh(int s, ShapeType st, btVector3 sp) : size(s),shapeType(st),shapeParams(sp),shape(nullptr){
    for(int i=0;i<Tabs::TABS_NUM;i++){
        tab[i]=nullptr;
        VBO[i]=0;
    }
}

sb::Mesh::~Mesh(){
    for(int i=0;i<Tabs::TABS_NUM;i++)
        delete []tab[i];
}


void sb::Mesh::setTab(Tabs tabId, const float *data){
    if(!tab[tabId])
        tab[tabId] = new float[size*TabSizes[tabId]];
    std::copy_n(data, size*TabSizes[tabId], tab[tabId]);
}

int sb::Mesh::getSize(){
    return size;
}


void sb::Mesh::initOpenGLPart(){
    for(int i=0;i<Tabs::TABS_NUM;i++)
        if(tab[i] && VBO[i]==0){
            glGenBuffers(1, &VBO[i]);
            glBindBuffer(GL_ARRAY_BUFFER, VBO[i]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(float)*size*TabSizes[i], tab[i], GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
}

void sb::Mesh::deinitOpenGLPart(){
    for(int i=0;i<Tabs::TABS_NUM;i++)
        if(VBO[i]){
            glDeleteBuffers(1, &VBO[i]);
            VBO[i]=0;
        }
}

void sb::Mesh::bindVBO(Tabs tabId){
    glBindBuffer(GL_ARRAY_BUFFER, VBO[tabId]);
}


void sb::Mesh::initBulletPart(){
    switch(shapeType){
        case CUBOID:
            shape = new btBoxShape(shapeParams);
            break;
        
        case SPHERE:
            shape = new btSphereShape(shapeParams.x());
            break;

        case CONE:
            shape = new btConeShape(shapeParams.x(), shapeParams.y());
            break;
        
        case CYLINDER:
            shape = new btCylinderShape(shapeParams);
            break;

        case CONVEX_HULL:
            shape = new btConvexHullShape(tab[VERTEX_TAB], size, 3*sizeof(float));
            break;
    }
}

void sb::Mesh::deinitBulletPart(){
    delete shape;
}

btConvexShape* sb::Mesh::getShape(){
    return shape;
}
