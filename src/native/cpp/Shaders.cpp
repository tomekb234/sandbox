#include <Shaders.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <glm/gtc/type_ptr.hpp>

uint sb::Shader::loadShaderCode(std::string code, GLenum type){
    //compile shader
    const char* cCode = code.c_str();
    int resp;
    unsigned int shaderId = glCreateShader(type);
    glShaderSource(shaderId, 1, &cCode, NULL);
    glCompileShader(shaderId);

    //check compilation result
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &resp);
    if(!resp){
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &resp);
        char *errorLog = new char[resp];
        glGetShaderInfoLog(shaderId, resp, NULL, errorLog);
        std::cerr << "ERROR: Can't compile shader:\n" << errorLog << std::endl;
        delete []errorLog;
        glDeleteShader(shaderId);
        return 0;
    }

    return shaderId;
}

sb::Shader::Shader(std::string vCode, std::string fCode){
    //load and compile both shaders
    uint vertexId = loadShaderCode(vCode, GL_VERTEX_SHADER);
    if(!vertexId)
        throw;
    uint fragmentId = loadShaderCode(fCode, GL_FRAGMENT_SHADER);
    if(!fragmentId){
        glDeleteShader(vertexId);
        throw;
    }

    //link shader program
    programId = glCreateProgram();
    glAttachShader(programId, vertexId);
    glAttachShader(programId, fragmentId);
    glLinkProgram(programId);
    glDetachShader(programId, vertexId);
    glDetachShader(programId, fragmentId);
    glDeleteShader(vertexId);
    glDeleteShader(fragmentId);

    int resp;
    glGetProgramiv(programId, GL_LINK_STATUS, &resp);
    if(resp==GL_FALSE){
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &resp);
        char *errorLog = new char[resp];
        glGetProgramInfoLog(programId, resp, NULL, errorLog);
        std::cerr << "ERROR: Can't link shader program:\n" << errorLog << std::endl;
        delete []errorLog;
        glDeleteProgram(programId);
        throw;
    }
}

sb::Shader::~Shader(){
    glDeleteProgram(programId);
}

void sb::Shader::activate(){
    glUseProgram(programId);
}


extern "C"{
    extern const char* geoPassVertexShaderCode;
    extern const char* geoPassFragmentShaderCode;
}

sb::GeoPassShader::GeoPassShader() : sb::Shader(geoPassVertexShaderCode, geoPassFragmentShaderCode){
    matrixMUniformId = glGetUniformLocation(programId, "matrixM");
    matrixMVPUniformId = glGetUniformLocation(programId, "matrixMVP");
    colorFilterUniformId = glGetUniformLocation(programId, "colorFilter");
}

void sb::GeoPassShader::setMatrixMVP(const glm::mat4 &matrix){
    glUniformMatrix4fv(matrixMVPUniformId, 1, GL_FALSE, glm::value_ptr(matrix));
}

void sb::GeoPassShader::setMatrixM(const glm::mat4 &matrix){
    glUniformMatrix4fv(matrixMUniformId, 1, GL_FALSE, glm::value_ptr(matrix));
}

void sb::GeoPassShader::setColorFilter(const glm::vec4 &vec){
    glUniform4fv(colorFilterUniformId, 1, glm::value_ptr(vec));
}


extern "C"{
    extern const char* geoPassFloorVertexShaderCode;
    extern const char* geoPassFloorFragmentShaderCode;
}

sb::GeoPassFloorShader::GeoPassFloorShader() : sb::Shader(geoPassFloorVertexShaderCode, geoPassFloorFragmentShaderCode){
    activate();
    glUniform1i(glGetUniformLocation(programId, "floorTexture"), 4);

    matrixVPUniformId = glGetUniformLocation(programId, "matrixVP");
}

void sb::GeoPassFloorShader::setMatrixVP(const glm::mat4 &matrix){
    glUniformMatrix4fv(matrixVPUniformId, 1, GL_FALSE, glm::value_ptr(matrix));
}


extern "C"{
    extern const char* lightPassVertexShaderCode;
    extern const char* lightPassFragmentShaderCode;
}

sb::LightPassShader::LightPassShader() : sb::Shader(lightPassVertexShaderCode, lightPassFragmentShaderCode){
    activate();
    glUniform1i(glGetUniformLocation(programId, "texPosition"), 0);
    glUniform1i(glGetUniformLocation(programId, "texNormal"), 1);
    glUniform1i(glGetUniformLocation(programId, "texDiffuse"), 2);

    cameraPosUniformId = glGetUniformLocation(programId, "wsCameraPosition");
    lightPosDirUniformId = glGetUniformLocation(programId, "lightPosDir");
    lightColorUniformId = glGetUniformLocation(programId, "lightColor");
    lightTypeUniformId = glGetUniformLocation(programId, "lightModePoint");
    lightBrightnessUniformId = glGetUniformLocation(programId, "lightBrightness");
}

void sb::LightPassShader::setCameraPos(const glm::vec3 &vec){
    glUniform3fv(cameraPosUniformId, 1, glm::value_ptr(vec));
}

void sb::LightPassShader::setLightPosDir(const glm::vec3 &vec){
    glUniform3fv(lightPosDirUniformId, 1, glm::value_ptr(vec));
}

void sb::LightPassShader::setLightColor(const glm::vec3 &vec){
    glUniform3fv(lightColorUniformId, 1, glm::value_ptr(vec));
}

void sb::LightPassShader::setLightMode(bool pointMode){
    glUniform1i(lightTypeUniformId, int(pointMode));
}

void sb::LightPassShader::setLightBrightness(float brightness){
    glUniform1f(lightBrightnessUniformId, brightness);
}


extern "C"{
    extern const char* lightPassVertexShaderCode;
    extern const char* lightPassAmbientFragmentShaderCode;
}

sb::LightPassAmbientShader::LightPassAmbientShader() : sb::Shader(lightPassVertexShaderCode, lightPassAmbientFragmentShaderCode){
    activate();
    glUniform1i(glGetUniformLocation(programId, "texNormal"), 1);
    glUniform1i(glGetUniformLocation(programId, "texDiffuse"), 2);

    backgroundColorUniformId = glGetUniformLocation(programId, "backgroundColor");
}

void sb::LightPassAmbientShader::setBackgroundColor(const glm::vec4 &vec){
    glUniform4fv(backgroundColorUniformId, 1, glm::value_ptr(vec));
}


extern "C"{
    extern const char* singleColorVertexShaderCode;
    extern const char* singleColorFragmentShaderCode;
}

sb::SingleColorShader::SingleColorShader() : sb::Shader(singleColorVertexShaderCode, singleColorFragmentShaderCode){
    matrixMVPUniformId = glGetUniformLocation(programId, "matrixMVP");
    colorUniformId = glGetUniformLocation(programId, "color");
}

void sb::SingleColorShader::setMatrixMVP(const glm::mat4 &matrix){
    glUniformMatrix4fv(matrixMVPUniformId, 1, GL_FALSE, glm::value_ptr(matrix));
}

void sb::SingleColorShader::setColor(const glm::vec4 &color){
    glUniform4fv(colorUniformId, 1, glm::value_ptr(color));
}


extern "C"{
    extern const char* lightPassVertexShaderCode;
    extern const char* postprocessFragmentShaderCode;
}

sb::PostprocessShader::PostprocessShader() : sb::Shader(lightPassVertexShaderCode, postprocessFragmentShaderCode){
    activate();
    glUniform1i(glGetUniformLocation(programId, "hdrFrame"), 3);
    
    gammaUniformId = glGetUniformLocation(programId, "gamma");
    glUniform1f(gammaUniformId, 2.2); //default gamma value
    yFlipUniformId = glGetUniformLocation(programId, "yflip");
}

void sb::PostprocessShader::setGamma(float gamma){
    glUniform1f(gammaUniformId, gamma);
}

void sb::PostprocessShader::setYFlip(bool flip){
    glUniform1i(yFlipUniformId, int(flip));
}

