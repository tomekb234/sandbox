#include <Camera.h>

#include <glm/gtc/matrix_transform.hpp>

const float pi = 3.14159265359;

const float maxPitch = 89.8*pi/180.0;
const float mouseSpeed = 0.008;
const float keyboardSpeed = 0.01;

sb::Camera::Camera() : Camera(glm::vec3(0.0,0.0,0.0), glm::vec3(0.0,0.0,-1.0)){ }

sb::Camera::Camera(glm::vec3 pos, glm::vec3 viewRay) : position(pos),ratio(1.0),fov(pi/2){
    viewRay = glm::normalize(viewRay);
    pitch = glm::asin(viewRay.y);
    yaw = glm::asin(viewRay.z/glm::sqrt(1.0-viewRay.y*viewRay.y));
    updateViewMatrix();
    updateProjectionMatrix();
}

sb::Camera::~Camera(){ }

void sb::Camera::updateViewMatrix(){
    direction = glm::vec3(glm::cos(yaw)*glm::cos(pitch), glm::sin(pitch), glm::sin(yaw)*glm::cos(pitch));
    viewMatrix = glm::lookAt(position, position+direction, glm::vec3(0.0f, 1.0f, 0.0f));
    combinedMatrix = projectionMatrix*viewMatrix;
}

void sb::Camera::updateProjectionMatrix(){
    projectionMatrix = glm::perspective(fov, ratio, 0.1f, 100.0f);
    combinedMatrix = projectionMatrix*viewMatrix;
}

void sb::Camera::setRatio(float r){
    ratio = r;
    updateProjectionMatrix();
}

void sb::Camera::setFOV(float f){
    fov = f;
    updateProjectionMatrix();
}

void sb::Camera::rotate(float xmov, float ymov){
    yaw = glm::mod(yaw+xmov*mouseSpeed, 2*pi);
    pitch = glm::clamp(pitch+ymov*mouseSpeed, -maxPitch, maxPitch);
    updateViewMatrix();
}

void sb::Camera::step(float forward, float right, float up){
    position += direction*(keyboardSpeed*forward);
    position += glm::normalize(glm::cross(direction, glm::vec3(0.0, 1.0, 0.0)))*(keyboardSpeed*right);
    position += glm::vec3(0.0f, 1.0f, 0.0f)*(keyboardSpeed*up);
    updateViewMatrix();
}

glm::mat4 sb::Camera::getTransform(){
    return combinedMatrix;
}

glm::vec3 sb::Camera::getPosition(){
    return position;
}

glm::vec3 sb::Camera::getDirection(){
    return direction;
}
