#include <SharedBuffer.h>

#include <iostream>
#include <algorithm>
#include <glad/glad.h>

sb::SharedBuffer::SharedBuffer(int w, int h) : width(w),height(h),FBO(0),RBOcolor(0),backReady(false),frontReady(false){
    frontBuffer = new unsigned char[4*width*height];
}

sb::SharedBuffer::~SharedBuffer(){
    delete []frontBuffer;
}

void sb::SharedBuffer::moveToFrontBuffer(){
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    glReadPixels(0,0,width,height,GL_BGRA,GL_UNSIGNED_BYTE,(void*)frontBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    frontReady.store(true);
    backReady=false;
}

void sb::SharedBuffer::initOpenGLPart(){
    glGenFramebuffers(1, &FBO);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);

    glGenRenderbuffers(1, &RBOcolor);
    glBindRenderbuffer(GL_RENDERBUFFER, RBOcolor);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, RBOcolor);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cerr << "ERROR: Framebuffer creation error!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void sb::SharedBuffer::deinitOpenGLPart(){
    glDeleteRenderbuffers(1, &RBOcolor);
    glDeleteFramebuffers(1, &FBO);
}

bool sb::SharedBuffer::waitBeforeRendering(){
    std::unique_lock<std::mutex> lock(bufferFlagMutex);
    return bufferFlagCond.wait_for(lock, std::chrono::milliseconds(20), [this]{
        return !frontReady || !backReady;
    });
}

void sb::SharedBuffer::preRender(){
    //assumption: frontReady=false or backReady=false
    if(backReady)
        moveToFrontBuffer();

    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    glViewport(0,0,width, height);
}

void sb::SharedBuffer::postRender(){
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    backReady = true;

    if(!frontReady.load())
        moveToFrontBuffer();
}

int sb::SharedBuffer::getWidth()const{
    return width;
}

int sb::SharedBuffer::getHeight()const{
    return height;
}

bool sb::SharedBuffer::needsYFlip()const{
    return true;
}

unsigned char* sb::SharedBuffer::getFrontBuffer(){
    return frontBuffer;
}

bool sb::SharedBuffer::nextFrameAvailable(){
    return frontReady.load();
}

void sb::SharedBuffer::nextFrameAcknowledge(){
    //assumtion: frontReady=true
    {
        std::lock_guard<std::mutex> lock(bufferFlagMutex);
        frontReady.store(false);
    }
    bufferFlagCond.notify_one();
}
