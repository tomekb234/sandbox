#include <PhysicsEngine.h>

#include <Entity.h>
#include <Rope.h>

sb::PhysicsEngine::PhysicsEngine() : state(OFF),timeFactor(1.0){
    collisionConfig = new btDefaultCollisionConfiguration();
    collisionDispatcher = new btCollisionDispatcher(collisionConfig);
    broadphaseInterface = new btDbvtBroadphase();
    constraintSolver = new btSequentialImpulseConstraintSolver();
    world = new btDiscreteDynamicsWorld(collisionDispatcher, broadphaseInterface, constraintSolver, collisionConfig);

    world->setGravity(btVector3(0.0, -9.81, 0.0));

    floorShape = new btStaticPlaneShape(btVector3(0.0, 1.0, 0.0), 0.0);
    floorBody = new btRigidBody(0.0, new btDefaultMotionState(), floorShape);
    floorBody->setFriction(1.0f);
    floorBody->setRestitution(1.0f);
    world->addRigidBody(floorBody);
}

sb::PhysicsEngine::~PhysicsEngine(){
    delete world;
    delete constraintSolver;
    delete broadphaseInterface;
    delete collisionDispatcher;
    delete collisionConfig;
    delete floorBody;
    delete floorShape;
}

void sb::PhysicsEngine::mainLoop(){
    simulatonActive = false;

    //start engine
    {
        std::lock_guard<std::mutex> lock(initMutex);
        state = ON;
    }
    initCond.notify_one();

    auto lastTime = std::chrono::high_resolution_clock::now();
    //main loop
    while(true){
        //process request queue
        while(true){
            if(state==CHANGING)
                break;
            if (!requestQueue.handle())
                break;
        }
        if(state==CHANGING)
            break;

        //simualte the world step
        auto currentTime = std::chrono::high_resolution_clock::now();
        auto timeStep = currentTime-lastTime;
        if(simulatonActive)
            world->stepSimulation(timeFactor*std::chrono::duration_cast<std::chrono::microseconds>(timeStep).count()*1e-6);
        if(timeStep < std::chrono::milliseconds(10))
            std::this_thread::sleep_for(std::chrono::milliseconds(10)-timeStep);
        lastTime = currentTime;
    }

    //stop engine
    state = OFF;
}

bool sb::PhysicsEngine::start(){
    if(state!=OFF)
        return false;
    
    //start physics thread
    state = CHANGING;
    physicsThread = std::thread(&PhysicsEngine::mainLoop, this);

    //wait for deinit completion
    std::unique_lock<std::mutex> lock(initMutex);
    initCond.wait(lock, [this]{return state!=CHANGING;});
    return state==ON;
}

void sb::PhysicsEngine::stop(){
    if(state!=ON)
        return;
    
    state = CHANGING;
    physicsThread.join();
}

void sb::PhysicsEngine::enqueueRequest(std::function<void()> request){
    requestQueue.push(request);
}

btVector3 sb::PhysicsEngine::getGravity(){
    return gravity;
}

int sb::PhysicsEngine::rayCast(btVector3 pos, btVector3 dir) {
    btVector3 vec = pos + 1000 * dir;
    btCollisionWorld::ClosestRayResultCallback result(pos, vec);
    world->rayTest(pos, vec, result);
    if (result.hasHit()) {
        for (sb::Entity* entity : entities) {
            if (entity->rigidBody == result.m_collisionObject)
                return entity->getId();
        }
    } return -1;
}

int sb::PhysicsEngine::entityCount() {
    return entities.size();
}


void sb::PhysicsEngine::addEntity(Entity* entity) {
    std::lock_guard<std::mutex>lock(entity->physicsLinkMutex);
    entity->initPhysicsPart(this);
    entities.push_back(entity);
    entity->physicsEngineIterator = std::prev(entities.end());
    world->addRigidBody(entity->rigidBody);
}

void sb::PhysicsEngine::removeEntity(Entity* entity) {
    std::lock_guard<std::mutex>lock(entity->physicsLinkMutex);
    entity->dumpDataFromPhysics();
    world->removeRigidBody(entity->rigidBody);
    entities.erase(entity->physicsEngineIterator);
    entity->deinitPhysicsPart();
}

void sb::PhysicsEngine::readdEntity(Entity* entity){
    if(entity->physicsLinkState != Entity::ON)
        return;
    world->removeRigidBody(entity->rigidBody);
    world->addRigidBody(entity->rigidBody);
    entity->updateAcceleration();
}

void sb::PhysicsEngine::addRope(Rope* rope) {
    Entity *a = nullptr, *b = nullptr;
    for (Entity* entity : entities) {
        if (entity->id == rope->entityIdA)
            a = entity;
        if (entity->id == rope->entityIdB)
            b = entity;
    }

    std::lock_guard<std::mutex> lock(rope->physicsLinkMutex);
    rope->physicsEngine = this;
    rope->bodyA = a->rigidBody;
    rope->bodyB = b->rigidBody;
    rope->initPhysicsPart();
    ropes.push_back(rope);
    rope->physicsEngineIterator = std::prev(ropes.end());

    world->addRigidBody(rope->endA);
    world->addRigidBody(rope->endB);
    world->addConstraint(rope->constrA);
    world->addConstraint(rope->constrB);
    world->addConstraint(rope->constr);
}

void sb::PhysicsEngine::removeRope(Rope* rope) {
    world->removeConstraint(rope->constr);
    world->removeConstraint(rope->constrA);
    world->removeConstraint(rope->constrB);
    world->removeRigidBody(rope->endA);
    world->removeRigidBody(rope->endB);

    std::lock_guard<std::mutex> lock(rope->physicsLinkMutex);
    rope->physicsEngine = nullptr;
    ropes.erase(rope->physicsEngineIterator);
    rope->deinitPhysicsPart();
}

void sb::PhysicsEngine::setGravity(btVector3 newGravity) {
    gravity = newGravity;
    for (Entity* entity : entities)
        entity->updateAcceleration();
}

void sb::PhysicsEngine::playPause(bool play) {
    if (play) {
        for (Rope* rope : ropes)
            rope->updateTransform();
    }

    simulatonActive = play;
}

void sb::PhysicsEngine::setTimeFactor(float factor) {
    timeFactor = factor;
}
