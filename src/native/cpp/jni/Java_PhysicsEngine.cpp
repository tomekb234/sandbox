#include <jni.h>
#include <PhysicsEngine.h>
#include <Entity.h>
#include <Rope.h>

namespace java {
    jfieldID PhysicsEngine_engine;
    extern jfieldID NativeEntity_entity;
    extern jfieldID NativeRope_rope;
    extern jfieldID Mesh_mesh;
    extern jfieldID Vector3f_x;
    extern jfieldID Vector3f_y;
    extern jfieldID Vector3f_z;
}

extern "C" {
    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_setup(JNIEnv* env, jclass cls) {
        java::PhysicsEngine_engine = env->GetFieldID(cls, "engine", "J");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_init(JNIEnv* env, jobject obj) {
        auto engine = new sb::PhysicsEngine();
        env->SetLongField(obj, java::PhysicsEngine_engine, (jlong)engine);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_start(JNIEnv* env, jobject obj) {
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        if (!engine->start())
            env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "failed to start physics engine");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_stop(JNIEnv* env, jobject obj) {
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        engine->stop();
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_registerMesh(JNIEnv* env, jobject, jobject meshObj){
        auto mesh = (sb::Mesh*)env->GetLongField(meshObj, java::Mesh_mesh);
        mesh->initBulletPart();
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_addEntity(JNIEnv* env, jobject obj, jobject entityObj){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        auto entity = (sb::Entity*)env->GetLongField(entityObj, java::NativeEntity_entity);
        entity->setTempPhysicsState(true);
        entity->link();
        engine->enqueueRequest([engine, entity] () { engine->addEntity(entity); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_removeEntity(JNIEnv* env, jobject obj, jobject entityObj){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        auto entity = (sb::Entity*)env->GetLongField(entityObj, java::NativeEntity_entity);
        entity->setTempPhysicsState(false);
        engine->enqueueRequest([engine, entity] () { engine->removeEntity(entity); entity->unlink(); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_addRope(JNIEnv* env, jobject obj, jobject ropeObj){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        auto rope = (sb::Rope*)env->GetLongField(ropeObj, java::NativeRope_rope);
        rope->link();
        engine->enqueueRequest([engine, rope] () { engine->addRope(rope); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_removeRope(JNIEnv* env, jobject obj, jobject ropeObj){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        auto rope = (sb::Rope*)env->GetLongField(ropeObj, java::NativeRope_rope);
        engine->enqueueRequest([engine, rope] () { engine->removeRope(rope); rope->unlink(); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_setGravity(JNIEnv* env, jobject obj, jobject gravity) {
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        float x = env->GetFloatField(gravity, java::Vector3f_x);
        float y = env->GetFloatField(gravity, java::Vector3f_y);
        float z = env->GetFloatField(gravity, java::Vector3f_z);
        btVector3 newGravity = btVector3(x,y,z);
        engine->enqueueRequest([engine, newGravity] () { engine->setGravity(newGravity); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_setPaused(JNIEnv* env, jobject obj, jboolean paused){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        engine->enqueueRequest([engine, paused] () { engine->playPause(!paused); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_PhysicsEngine_setSpeed(JNIEnv* env, jobject obj, jfloat speed){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        engine->enqueueRequest([engine, speed] () { engine->setTimeFactor(speed); });
    }

    JNIEXPORT jint JNICALL Java_sandbox_engine_PhysicsEngine_rayCast(JNIEnv* env, jobject obj, jobject pos, jobject dir){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        float px = env->GetFloatField(pos, java::Vector3f_x);
        float py = env->GetFloatField(pos, java::Vector3f_y);
        float pz = env->GetFloatField(pos, java::Vector3f_z);
        float dx = env->GetFloatField(dir, java::Vector3f_x);
        float dy = env->GetFloatField(dir, java::Vector3f_y);
        float dz = env->GetFloatField(dir, java::Vector3f_z);
        return engine->rayCast(btVector3(px,py,pz), btVector3(dx,dy,dz));
    }

    JNIEXPORT jint JNICALL Java_sandbox_engine_PhysicsEngine_entityCount(JNIEnv* env, jobject obj){
        auto engine = (sb::PhysicsEngine*)env->GetLongField(obj, java::PhysicsEngine_engine);
        return engine->entityCount();
    }
}
