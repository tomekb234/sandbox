#include <jni.h>
#include <Entity.h>

namespace java{
    jfieldID NativeEntity_entity;
    extern jfieldID Mesh_mesh;
    extern jclass Vector3f;
    extern jfieldID Vector3f_x;
    extern jfieldID Vector3f_y;
    extern jfieldID Vector3f_z;
    extern jmethodID Vector3f_constructor;
}

extern "C" {
    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setup(JNIEnv* env, jclass cls){
        java::NativeEntity_entity = env->GetFieldID(cls, "entity", "J");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_init(JNIEnv* env, jobject obj){
        auto entity = new sb::Entity();
        env->SetLongField(obj, java::NativeEntity_entity, (jlong)entity);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_initMesh(JNIEnv* env, jobject obj, jobject meshObj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        auto mesh = (sb::Mesh*)env->GetLongField(meshObj, java::Mesh_mesh);
        entity->initMesh(mesh);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_deinit(JNIEnv* env, jobject obj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->unlink();
    }

    JNIEXPORT jint JNICALL Java_sandbox_engine_NativeEntity_getId(JNIEnv* env, jobject obj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        return entity->getId();
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setColor(JNIEnv* env, jobject obj, jobject color){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        float r = env->GetFloatField(color, java::Vector3f_x);
        float g = env->GetFloatField(color, java::Vector3f_y);
        float b = env->GetFloatField(color, java::Vector3f_z);
        entity->setColor(glm::vec4(r,g,b,sb::nan));
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setColorOpacity(JNIEnv* env, jobject obj, jfloat colorOpacity){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setColor(glm::vec4(sb::nan,sb::nan,sb::nan,colorOpacity));
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setAcceleration(JNIEnv* env, jobject obj, jobject acceleration){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        float x = env->GetFloatField(acceleration, java::Vector3f_x);
        float y = env->GetFloatField(acceleration, java::Vector3f_y);
        float z = env->GetFloatField(acceleration, java::Vector3f_z);
        entity->setAcceleration(glm::vec3(x,y,z));
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setMass(JNIEnv* env, jobject obj, jfloat mass){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setMass(mass);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setImmovable(JNIEnv* env, jobject obj, jboolean immovable){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setImmovable(immovable);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setFriction(JNIEnv* env, jobject obj, jfloat friction){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setFriction(friction);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setBounciness(JNIEnv* env, jobject obj, jfloat bounciness){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setRestitution(bounciness);
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_NativeEntity_getPosition(JNIEnv* env, jobject obj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        auto posVec = entity->getPosition();
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, posVec.x, posVec.y, posVec.z);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setPosition(JNIEnv* env, jobject obj, jobject position){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        float x = env->GetFloatField(position, java::Vector3f_x);
        float y = env->GetFloatField(position, java::Vector3f_y);
        float z = env->GetFloatField(position, java::Vector3f_z);
        entity->setPosition(glm::vec3(x,y,z));
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_NativeEntity_getRotation(JNIEnv* env, jobject obj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        auto rotVec = entity->getAngularPosition();
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, rotVec.x, rotVec.y, rotVec.z);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setRotation(JNIEnv* env, jobject obj, jobject rotation){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        float x = env->GetFloatField(rotation, java::Vector3f_x);
        float y = env->GetFloatField(rotation, java::Vector3f_y);
        float z = env->GetFloatField(rotation, java::Vector3f_z);
        entity->setAngularPosition(glm::vec3(x,y,z));
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_NativeEntity_getVelocity(JNIEnv* env, jobject obj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        auto speedVec = entity->getVelocity();
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, speedVec.x, speedVec.y, speedVec.z);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setVelocity(JNIEnv* env, jobject obj, jobject velocity){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        float x = env->GetFloatField(velocity, java::Vector3f_x);
        float y = env->GetFloatField(velocity, java::Vector3f_y);
        float z = env->GetFloatField(velocity, java::Vector3f_z);
        entity->setVelocity(glm::vec3(x,y,z));
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_NativeEntity_getAngularVelocity(JNIEnv* env, jobject obj){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        auto angSpeedVec = entity->getAngularVelocity();
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, angSpeedVec.x, angSpeedVec.y, angSpeedVec.z);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setAngularVelocity(JNIEnv* env, jobject obj, jobject angularVelocity){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        float x = env->GetFloatField(angularVelocity, java::Vector3f_x);
        float y = env->GetFloatField(angularVelocity, java::Vector3f_y);
        float z = env->GetFloatField(angularVelocity, java::Vector3f_z);
        entity->setAngularVelocity(glm::vec3(x,y,z));
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setSize(JNIEnv* env, jobject obj, jfloat size){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setSize(size);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeEntity_setSelected(JNIEnv* env, jobject obj, jboolean selected){
        auto entity = (sb::Entity*)env->GetLongField(obj, java::NativeEntity_entity);
        entity->setSelected(selected);
    }
}
