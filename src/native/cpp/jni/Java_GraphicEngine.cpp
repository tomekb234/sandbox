#include <jni.h>
#include <GraphicEngine.h>
#include <SharedBuffer.h>
#include <OwnWindow.h>
#include <Entity.h>
#include <Mesh.h>
#include <Light.h>

namespace java {
    jfieldID GraphicEngine_engine;
    jfieldID GraphicEngine_buffer;
    extern jfieldID NativeEntity_entity;
    extern jfieldID Mesh_mesh;
    extern jfieldID NativeLight_light;
    extern jfieldID NativeLight_type;
    extern jclass Vector3f;
    extern jfieldID Vector3f_x;
    extern jfieldID Vector3f_y;
    extern jfieldID Vector3f_z;
    extern jmethodID Vector3f_constructor;
}

extern "C" {
    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_setup(JNIEnv* env, jclass cls) {
        java::GraphicEngine_engine = env->GetFieldID(cls, "engine", "J");
        java::GraphicEngine_buffer = env->GetFieldID(cls, "buffer", "J");
    }


    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_init(JNIEnv* env, jobject obj) {
        auto engine = new sb::GraphicEngine();
        env->SetLongField(obj, java::GraphicEngine_engine, (jlong)engine);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_start(JNIEnv* env, jobject obj) {
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        if (!engine->start())
            env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "failed to start graphic engine");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_stop(JNIEnv* env, jobject obj) {
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        engine->stop();
    }


    JNIEXPORT jobject JNICALL Java_sandbox_engine_GraphicEngine_newBuffer(JNIEnv* env, jobject obj, jint width, jint height, jboolean ownWindow) {
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto buffer = (sb::RenderTarget*)env->GetLongField(obj, java::GraphicEngine_buffer);
        if (buffer)
            buffer->unlink();
        sb::RenderTarget *newBuffer;
        if(ownWindow)
            newBuffer = new sb::OwnWindow(width, height, engine);
        else
            newBuffer = new sb::SharedBuffer(width, height);
        newBuffer->link();
        engine->enqueueRequest([engine, newBuffer] () { engine->changeTarget(newBuffer); });
        env->SetLongField(obj, java::GraphicEngine_buffer, (jlong)newBuffer);
        return ownWindow ? ((jobject)NULL) : (env->NewDirectByteBuffer(((sb::SharedBuffer*)newBuffer)->getFrontBuffer(), 4*width*height));
    }

    JNIEXPORT jboolean JNICALL Java_sandbox_engine_GraphicEngine_nextFrameAvailable(JNIEnv* env, jobject obj) {
        auto buffer = (sb::SharedBuffer*)env->GetLongField(obj, java::GraphicEngine_buffer);
        if (buffer)
            return buffer->nextFrameAvailable();
        return false;
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_nextFrameAcknowledge(JNIEnv* env, jobject obj){
        auto buffer = (sb::SharedBuffer*)env->GetLongField(obj, java::GraphicEngine_buffer);
        if (buffer)
            buffer->nextFrameAcknowledge();
    }


    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_registerMesh(JNIEnv* env, jobject obj, jobject meshObj){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto mesh = (sb::Mesh*)env->GetLongField(meshObj, java::Mesh_mesh);
        engine->enqueueRequest([engine, mesh] () { engine->registerMesh(mesh); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_addEntity(JNIEnv* env, jobject obj, jobject entityObj){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto entity = (sb::Entity*)env->GetLongField(entityObj, java::NativeEntity_entity);
        entity->link();
        engine->enqueueRequest([engine, entity] () { engine->addEntity(entity); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_removeEntity(JNIEnv* env, jobject obj, jobject entityObj){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto entity = (sb::Entity*)env->GetLongField(entityObj, java::NativeEntity_entity);
        engine->enqueueRequest([engine, entity] () { engine->removeEntity(entity); entity->unlink(); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_addLight(JNIEnv* env, jobject obj, jobject light){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        int type = env->GetIntField(light, java::NativeLight_type);

        if(type==0){
            auto pl = (sb::PointLight*)env->GetLongField(light, java::NativeLight_light);
            pl->link();
            engine->enqueueRequest([engine, pl] () { engine->addPointLight(pl); });
        }
        else{
            auto dl = (sb::DirectionalLight*)env->GetLongField(light, java::NativeLight_light);
            dl->link();
            engine->enqueueRequest([engine, dl] () { engine->addDirectionalLight(dl); });
        }
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_removeLight(JNIEnv* env, jobject obj, jobject light){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        int type = env->GetIntField(light, java::NativeLight_type);

        if(type==0){
            auto pl = (sb::PointLight*)env->GetLongField(light, java::NativeLight_light);
            engine->enqueueRequest([engine, pl] () { engine->removePointLight(pl); pl->unlink(); });
        }
        else{
            auto dl = (sb::DirectionalLight*)env->GetLongField(light, java::NativeLight_light);
            engine->enqueueRequest([engine, dl] () { engine->removeDirectionalLight(dl); dl->unlink(); });
        }
    }


    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_setBackgroundColor(JNIEnv* env, jobject obj, jobject color) {
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        float r = env->GetFloatField(color, java::Vector3f_x);
        float g = env->GetFloatField(color, java::Vector3f_y);
        float b = env->GetFloatField(color, java::Vector3f_z);
        glm::vec4 background = glm::vec4(r,g,b,sb::nan);
        engine->enqueueRequest([engine, background] () { engine->changeBackground(background); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_setGammaCorrection(JNIEnv* env, jobject obj, jfloat gamma){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        engine->enqueueRequest([engine, gamma] () { engine->setGamma(gamma); });
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_setBrightness(JNIEnv* env, jobject obj, jfloat brightness){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        glm::vec4 background = glm::vec4(sb::nan,sb::nan,sb::nan,brightness);
        engine->enqueueRequest([engine, background] () { engine->changeBackground(background); });
    }


    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_mouseMoved(JNIEnv* env, jobject obj, jint dx, jint dy){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        engine->getCamera()->rotate(dx, -dy);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_keyPressed(JNIEnv* env, jobject obj, jint key){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        engine->setKeyState((sb::GraphicEngine::Key)key, true);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_keyReleased(JNIEnv* env, jobject obj, jint key){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        engine->setKeyState((sb::GraphicEngine::Key)key, false);
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_GraphicEngine_getCameraPosition(JNIEnv* env, jobject obj){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto pos = engine->getCamera()->getPosition();
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, pos.x, pos.y, pos.z);
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_GraphicEngine_getCameraDirection(JNIEnv* env, jobject obj){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto dir = engine->getCamera()->getDirection();
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, dir.x, dir.y, dir.z);
    }

    JNIEXPORT jobject JNICALL Java_sandbox_engine_GraphicEngine_mouseDirection(JNIEnv* env, jobject obj, jint x, jint y){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        auto dir = engine->mouseDirection(x, y);
        return env->NewObject(java::Vector3f, java::Vector3f_constructor, dir.x, dir.y, dir.z);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_GraphicEngine_setFpsLimit(JNIEnv* env, jobject obj, jint limit){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        engine->enqueueRequest([engine,limit] () { engine->setFpsLimit(limit); });
    }

    JNIEXPORT jint JNICALL Java_sandbox_engine_GraphicEngine_fps(JNIEnv* env, jobject obj){
        auto engine = (sb::GraphicEngine*)env->GetLongField(obj, java::GraphicEngine_engine);
        return int(engine->getFps());
    }
}
