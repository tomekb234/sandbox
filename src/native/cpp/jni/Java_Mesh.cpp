#include <jni.h>
#include <Mesh.h>

namespace java{
    jfieldID Mesh_mesh;
    extern jfieldID Vector3f_x;
    extern jfieldID Vector3f_y;
    extern jfieldID Vector3f_z;
}

extern "C" {
    JNIEXPORT void JNICALL Java_sandbox_engine_Mesh_setup(JNIEnv* env, jclass cls){
        java::Mesh_mesh = env->GetFieldID(cls, "mesh", "J");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_Mesh_init(JNIEnv* env, jobject obj, jint shapeType, jobject shapeParams, jint size){
        float x = env->GetFloatField(shapeParams, java::Vector3f_x);
        float y = env->GetFloatField(shapeParams, java::Vector3f_y);
        float z = env->GetFloatField(shapeParams, java::Vector3f_z);
        auto mesh = new sb::Mesh(size, (sb::Mesh::ShapeType)shapeType, btVector3(x, y, z));
        env->SetLongField(obj, java::Mesh_mesh, (jlong)mesh);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_Mesh_loadData(JNIEnv* env, jobject obj, jint tabType, jfloatArray arr){
        auto mesh = (sb::Mesh*)env->GetLongField(obj, java::Mesh_mesh);
        float* array = env->GetFloatArrayElements(arr, 0);

        mesh->setTab((sb::Mesh::Tabs)tabType, array);

        env->ReleaseFloatArrayElements(arr, array, JNI_ABORT);
    }
}
