#include <jni.h>

namespace java{
    jclass Vector3f;
    jmethodID Vector3f_constructor;
    jfieldID Vector3f_x;
    jfieldID Vector3f_y;
    jfieldID Vector3f_z;
}

extern "C"{
    JNIEXPORT void JNICALL Java_sandbox_engine_Vector3f_setup(JNIEnv* env, jclass cls){
        java::Vector3f = (jclass)env->NewGlobalRef(cls);
        java::Vector3f_constructor = env->GetMethodID(cls, "<init>", "(FFF)V");
        java::Vector3f_x = env->GetFieldID(cls, "x", "F");
        java::Vector3f_y = env->GetFieldID(cls, "y", "F");
        java::Vector3f_z = env->GetFieldID(cls, "z", "F");
    }
}
