#include <jni.h>
#include <Light.h>

namespace java{
    jfieldID NativeLight_light;
    jfieldID NativeLight_type;
    extern jclass Vector3f;
    extern jfieldID Vector3f_x;
    extern jfieldID Vector3f_y;
    extern jfieldID Vector3f_z;
}

extern "C" {
    JNIEXPORT void JNICALL Java_sandbox_engine_NativeLight_setup(JNIEnv* env, jclass cls){
        java::NativeLight_light = env->GetFieldID(cls, "light", "J");
        java::NativeLight_type = env->GetFieldID(cls, "type", "I");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeLight_init(JNIEnv* env, jobject obj, jint type) {
        env->SetIntField(obj, java::NativeLight_type, type);
        if(type==0) //point light
            env->SetLongField(obj, java::NativeLight_light, (jlong)new sb::PointLight());
        else //directional light
            env->SetLongField(obj, java::NativeLight_light, (jlong)new sb::DirectionalLight());
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeLight_deinit(JNIEnv* env, jobject obj, jint type) {
        ((sb::Light*)env->GetLongField(obj, java::NativeLight_light))->unlink();
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeLight_setColor(JNIEnv* env, jobject obj, jobject color){
        glm::vec3 col;
        col.x = env->GetFloatField(color, java::Vector3f_x);
        col.y = env->GetFloatField(color, java::Vector3f_y);
        col.z = env->GetFloatField(color, java::Vector3f_z);

        ((sb::Light*)env->GetLongField(obj, java::NativeLight_light))->color=col;
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeLight_setBrightness(JNIEnv* env, jobject obj, jfloat brightness){
        int type = env->GetIntField(obj, java::NativeLight_type);
        ((sb::Light*)env->GetLongField(obj, java::NativeLight_light))->brightness = (type==0)?brightness:(brightness/20);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeLight_setVector(JNIEnv* env, jobject obj, jobject vector){
        int type = env->GetIntField(obj, java::NativeLight_type);
        glm::vec3 vec;
        vec.x = env->GetFloatField(vector, java::Vector3f_x);
        vec.y = env->GetFloatField(vector, java::Vector3f_y);
        vec.z = env->GetFloatField(vector, java::Vector3f_z);

        if(type==0)
            ((sb::PointLight*)env->GetLongField(obj, java::NativeLight_light))->position=vec;
        else
            ((sb::DirectionalLight*)env->GetLongField(obj, java::NativeLight_light))->direction=glm::normalize(vec);
    }
}
