#include <jni.h>
#include <Rope.h>

namespace java{
    jfieldID NativeRope_rope;
}

extern "C" {
    JNIEXPORT void JNICALL Java_sandbox_engine_NativeRope_setup(JNIEnv* env, jclass cls){
        java::NativeRope_rope = env->GetFieldID(cls, "rope", "J");
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeRope_init(JNIEnv* env, jobject obj, jint entityIdA, jint entityIdB) {
        auto rope = new sb::Rope(entityIdA, entityIdB);
        env->SetLongField(obj, java::NativeRope_rope, (jlong)rope);
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeRope_deinit(JNIEnv* env, jobject obj) {
        auto rope = (sb::Rope*)env->GetLongField(obj, java::NativeRope_rope);
        rope->unlink();
    }

    JNIEXPORT void JNICALL Java_sandbox_engine_NativeRope_setLength(JNIEnv* env, jobject obj, jfloat length) {
        auto rope = (sb::Rope*)env->GetLongField(obj, java::NativeRope_rope);
        rope->setLength(length);
    }
}
