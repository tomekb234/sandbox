#include <Entity.h>

#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

sb::MotionState::MotionState(glm::mat4 *trans) : transform(trans){ }

void sb::MotionState::getWorldTransform(btTransform &comTransform) const{
    comTransform.setFromOpenGLMatrix(glm::value_ptr(*transform));
}

void sb::MotionState::setWorldTransform(const btTransform &comTransform){
    comTransform.getOpenGLMatrix(glm::value_ptr(*transform));
}


int nextId = 1;

sb::Entity::Entity() : id(nextId++), transform(1.0), mesh(nullptr) { }

sb::Entity::~Entity() { }

void sb::Entity::initMesh(Mesh* m) {
    mesh = m;
}

void sb::Entity::initGraphicsPart(sb::GraphicEngine *engine){
    graphicsEngine = engine;
    
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    mesh->bindVBO(Mesh::VERTEX_TAB);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(0);
    mesh->bindVBO(Mesh::COLOR_TAB);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(1);
    mesh->bindVBO(Mesh::NORMAL_TAB);
    glad_glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void sb::Entity::deinitGraphicsPart(){
    glDeleteVertexArrays(1, &VAO);
}

void sb::Entity::initPhysicsPart(PhysicsEngine *engine){
    physicsEngine = engine;
    motionState = new MotionState(&transform);
    rigidBody = new btRigidBody(0.0, motionState, mesh->getShape(), btVector3());
    rigidBody->setActivationState(DISABLE_DEACTIVATION);
    physicsLinkState = ON;
    updatePosition();
    updateAngularPosition();
    updateVelocity();
    updateAngularVelocity();
    updateInertia();
    updateFriction();
    updateRestitution();
    updateAcceleration();
}

void sb::Entity::dumpDataFromPhysics(){
    if(isnan(positionToSet.x))
        positionToSet = Vec(rigidBody->getCenterOfMassPosition());
    if(isnan(angularPositionToSet.x))
        angularVelocityToSet = Euler(rigidBody->getOrientation());
    if(isnan(velocityToSet.x))
        velocityToSet = Vec(rigidBody->getLinearVelocity());
    if(isnan(angularVelocityToSet.x))
        angularVelocityToSet = Vec(rigidBody->getAngularVelocity());
}

void sb::Entity::deinitPhysicsPart(){
    physicsLinkState = OFF;
    delete rigidBody;
    rigidBody=nullptr;
    delete motionState;
    physicsEngine=nullptr;
    if (ownShape)
        delete ownShape;
    ownShape = nullptr;
}

void sb::Entity::updatePosition(){
    if((physicsLinkState!=ON && physicsLinkState!=UNLINKING) || isnan(positionToSet.x))
        return;
    btTransform trans = rigidBody->getCenterOfMassTransform();
    trans.setOrigin(Vec(positionToSet));
    transform[3] = glm::vec4(positionToSet, 1.0);
    rigidBody->setCenterOfMassTransform(trans);
    positionToSet = glm::vec3(nan);
}

void sb::Entity::updateAngularPosition(){
    if((physicsLinkState!=ON && physicsLinkState!=UNLINKING) || isnan(angularPositionToSet.x))
        return;
    btTransform trans = rigidBody->getCenterOfMassTransform();
    trans.setRotation(Euler(angularPositionToSet));
    trans.getOpenGLMatrix(glm::value_ptr(transform));
    rigidBody->setCenterOfMassTransform(trans);
    angularPositionToSet = glm::vec3(nan);
}

void sb::Entity::updateVelocity(){
    if(physicsLinkState!=ON || isnan(velocityToSet.x))
        return;
    rigidBody->setLinearVelocity(Vec(velocityToSet));
    velocityToSet = glm::vec3(nan);
}

void sb::Entity::updateAngularVelocity(){
    if(physicsLinkState!=ON || isnan(angularVelocityToSet.x))
        return;
    rigidBody->setAngularVelocity(Vec(angularVelocityToSet));
    angularVelocityToSet = glm::vec3(sb::nan);
}

void sb::Entity::updateInertia(){
    if (physicsLinkState!=ON)
        return;
    if(immovable)
        rigidBody->setMassProps(0.0, btVector3(0.0f,0.0f,0.0f));
    else{
        btVector3 inertia;
        rigidBody->getCollisionShape()->calculateLocalInertia(mass, inertia);
        rigidBody->setMassProps(mass, inertia);
    } rigidBody->updateInertiaTensor();
}

void sb::Entity::updateFriction() {
    if (physicsLinkState!=ON)
        return;
    rigidBody->setFriction(friction);
}

void sb::Entity::updateRestitution() {
    if (physicsLinkState!=ON)
        return;
    rigidBody->setRestitution(restitution);
}

void sb::Entity::updateAcceleration() {
    if (physicsLinkState!=ON)
        return;
    btVector3 globalGravity = physicsEngine->getGravity();
    rigidBody->setGravity(globalGravity+Vec(acceleration));
}

void sb::Entity::updateSize() {
    if (physicsLinkState!=ON)
        return;
    if (size == 1.0f) {
        rigidBody->setCollisionShape(mesh->getShape());
        if (ownShape)
            delete ownShape;
        ownShape = nullptr;
    } else {
        btCollisionShape* shape = new btUniformScalingShape(mesh->getShape(), size);
        rigidBody->setCollisionShape(shape);
        if (ownShape)
            delete ownShape;
        ownShape = shape;
    }
}

int sb::Entity::getId() {
    return id;
}

glm::vec3 sb::Entity::getPosition(){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    if(!isnan(positionToSet.x))
        return positionToSet;
    else if(physicsLinkState==ON || physicsLinkState==UNLINKING)
        return Vec(rigidBody->getCenterOfMassPosition());
    else
        return glm::vec3(sb::nan);
}

void sb::Entity::setPosition(glm::vec3 newPos){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    positionToSet=newPos;
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this, engine = physicsEngine] {
            updatePosition();
            engine->readdEntity(this);
            unlink();
        });
    } else
        transform[3] = glm::vec4(newPos, 1.0);
}

glm::vec3 sb::Entity::getAngularPosition(){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    if(!isnan(angularPositionToSet.x))
        return angularPositionToSet;
    else if(physicsLinkState==ON || physicsLinkState==UNLINKING)
        return Euler(rigidBody->getOrientation());
    else
        return glm::vec3(sb::nan);
}

void sb::Entity::setAngularPosition(glm::vec3 newAngPos){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    angularPositionToSet=newAngPos;
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this] { updateAngularPosition(); unlink(); });
    } else
        btTransform(Euler(newAngPos), Vec(transform[3])).getOpenGLMatrix(glm::value_ptr(transform));
}

glm::vec3 sb::Entity::getVelocity(){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    if(!isnan(velocityToSet.x))
        return velocityToSet;
    else if(physicsLinkState==ON || physicsLinkState==UNLINKING)
        return Vec(rigidBody->getLinearVelocity());
    else
        return glm::vec3(sb::nan);
}

void sb::Entity::setVelocity(glm::vec3 newVel){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    velocityToSet = newVel;
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this] { updateVelocity(); unlink(); });
    }
}

glm::vec3 sb::Entity::getAngularVelocity(){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    if(!isnan(angularVelocityToSet.x))
        return angularVelocityToSet;
    else if(physicsLinkState==ON || physicsLinkState==UNLINKING)
        return Vec(rigidBody->getAngularVelocity());
    else
        return glm::vec3(sb::nan);
}

void sb::Entity::setAngularVelocity(glm::vec3 newAngVel){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    angularVelocityToSet = newAngVel;
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this] { updateAngularVelocity(); unlink(); });
    }
}

void sb::Entity::setColor(glm::vec4 newColor){
    if(!isnan(newColor.r)){
        color.r = newColor.r;
        color.g = newColor.g;
        color.b = newColor.b;
    }
    if(!isnan(newColor.a))
        color.a = newColor.a;
}

void sb::Entity::setMass(float newMass){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    mass = glm::max(newMass, 0.0f);
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this, engine = physicsEngine] { updateInertia(); engine->readdEntity(this); unlink(); });
    }
}

void sb::Entity::setImmovable(bool newState){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    immovable = newState;
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this, engine = physicsEngine] { updateInertia(); engine->readdEntity(this); unlink(); });
    }
}

void sb::Entity::setFriction(float newFriction) {
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    friction = glm::max(newFriction, 0.0f);
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this] { updateFriction(); unlink(); });
    }
}

void sb::Entity::setRestitution(float newRestitution) {
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    restitution = glm::max(newRestitution, 0.0f);
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this] { updateRestitution(); unlink(); });
    }
}

void sb::Entity::setAcceleration(glm::vec3 newAcceleration){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    acceleration = newAcceleration;
    if(physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this] { updateAcceleration(); unlink(); });
    }
}

void sb::Entity::setSize(float newSize) {
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    size = newSize;
    if (physicsLinkState==ON) {
        link();
        physicsEngine->enqueueRequest([this, engine = physicsEngine] { updateSize(); updateInertia(); engine->readdEntity(this); unlink(); });
    }
}

void sb::Entity::setSelected(bool newSelected) {
    selected = newSelected;
}

void sb::Entity::setTempPhysicsState(bool link){
    std::lock_guard<std::mutex>lock(physicsLinkMutex);
    if(link)
        physicsLinkState = LINKING;
    else
        physicsLinkState = UNLINKING;
}
