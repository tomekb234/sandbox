#include <Rope.h>

btEmptyShape emptyShape;

sb::Rope::Rope(int entityIdA, int entityIdB) : entityIdA(entityIdA), entityIdB(entityIdB) { }

void sb::Rope::initPhysicsPart() {
    float mass = 0.01;
    btVector3 inertia(1, 1, 1);

    msA = new btDefaultMotionState(bodyA->getCenterOfMassTransform());
    endA = new btRigidBody(mass, msA, &emptyShape, inertia);

    msB = new btDefaultMotionState(bodyB->getCenterOfMassTransform());
    endB = new btRigidBody(mass, msB, &emptyShape, inertia);

    btVector3 zero;
    zero.setZero();
    constrA = new btPoint2PointConstraint(*endA, *bodyA, zero, zero);
    constrB = new btPoint2PointConstraint(*endB, *bodyB, zero, zero);

    btTransform id;
    id.setIdentity();
    constr = new btSliderConstraint(*endA, *endB, id, id, true);

    updateTransform();
    updateLength();
}

void sb::Rope::deinitPhysicsPart() {
    delete constr;
    delete constrA;
    delete constrB;
    delete endA;
    delete endB;
    delete msA;
    delete msB;
}

void sb::Rope::updateTransform() {
    btVector3 a = bodyA->getCenterOfMassPosition();
    btVector3 b = bodyB->getCenterOfMassPosition();

    btVector3 d = (b - a).normalized();
    float yaw = atan2(d.z(), d.x());
    float pitch = atan2(d.y(), sqrt(d.x() * d.x() + d.z() * d.z()));
    btQuaternion rotation(-yaw, 0, pitch);

    btTransform transformA = endA->getCenterOfMassTransform();
    transformA.setRotation(rotation);
    endA->setCenterOfMassTransform(transformA);

    btTransform transformB = endB->getCenterOfMassTransform();
    transformB.setRotation(rotation);
    endB->setCenterOfMassTransform(transformB);
}

void sb::Rope::updateLength() {
    if (physicsEngine)
        constr->setUpperLinLimit(length);
}

void sb::Rope::setLength(float newLength) {
    std::lock_guard<std::mutex> lock(physicsLinkMutex);
    length = newLength;
    if (physicsEngine) {
        link();
        physicsEngine->enqueueRequest([this] () { updateLength(); unlink(); });
    }
}
