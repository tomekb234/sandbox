# Simple physics sandbox

Features:

- basic rigid bodies (cube, sphere, etc.)
- modifiable physical properties (mass, size, friction, etc.)
- point and directional lights
- linking objects with ropes
- pausing and changing speed of the simulation
- reverting simulation state
- grouping objects in a tree hierarchy
- reading and writing to XML files
- loading triangulated PLY models

Navigation:

- use toolbar buttons or edit menu to add objects
- select objects from left tree view
- select multiple objects (hold ctrl or shift) to group them
- drag and drop objects to move them in the tree hierarchy
- modify object properties with right panel
- left-click on the viewport to select object pointed by the cursor
- right-click on the viewport to capture mouse
- W/S/A/D - move around
- space/shift - move up/down

Used libraries:

- Bullet physics engine
- OpenGL
- JavaFX

## Building

Prerequisites:

- JDK (version >= 11)
- Ant (version >= 1.10)
- C++ compiler
- Make

Required packages (Debian/Ubuntu):

- `pkg-config`
- `libglfw3-dev`
- `libgm-dev`
- `libbullet-dev`
- `python3-glad` (can also be installed with `pip3`)

Build command: `ant` (downloads JavaFX libraries at first build)

## Running

(required OpenGL version >= 3.3)

Run command: `ant run`

Running directly: <br/>
`java -p build/java:build/lib -cp res` <br/>
`-Dsandbox.lib=build/native/sandbox.so` <br/>
`-m sandbox/sandbox.Main`

---

Icons from [icons8.com](https://icons8.com)
