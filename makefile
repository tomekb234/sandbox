# native-side build file

sourceDir := src/native/cpp
headersDir := src/native/include
outDir := build/native
genDir := build/gen
shaderDir := src/native/shaders
java ?= /usr/lib/jvm/default-java

headers := $(shell find $(headersDir) -name "*.h")
genHeaders := $(genDir)/include/glad/glad.h
sources := $(wildcard $(sourceDir)/*.cpp) $(wildcard $(sourceDir)/jni/*.cpp)
objects := $(sources:$(sourceDir)/%.cpp=$(outDir)/%.o)
genObjects := $(outDir)/glad.o
shaderObjects := $(patsubst %.glsl,$(outDir)/%.glsl.o, $(notdir $(wildcard $(shaderDir)/*.glsl)))

cpp := c++ -fpic -g
cppFlags := -I $(java)/include -I $(java)/include/linux -I $(headersDir) -I $(genDir)/include $(shell pkg-config --cflags bullet)
linkFlags := -ldl -lpthread $(shell pkg-config --libs bullet glfw3)

libTarget := $(outDir)/sandbox.so

all: dir $(libTarget)

dir:
	@mkdir -p $(outDir)
	@mkdir -p $(outDir)/jni
	@mkdir -p $(genDir)

$(genDir)/include/glad/glad.h $(genDir)/src/glad.c &:
	glad --profile core --generator c --spec gl --api "gl=3.3" --extensions "" --quiet --out-path $(genDir)

$(outDir)/glad.o: $(genDir)/src/glad.c $(genDir)/include/glad/glad.h
	$(cpp) $(cppFlags) -c $< -o $@

$(outDir)/%.o: $(sourceDir)/%.cpp $(headers) $(genHeaders)
	$(cpp) $(cppFlags) -c $< -o $@

$(libTarget): $(objects) $(genObjects) $(shaderObjects)
	$(cpp) -shared $^ -o $@ $(linkFlags)

$(libTarget:%.so=%.out): $(objects) $(genObjects) $(shaderObjects)
	$(cpp) $^ -o $@ $(linkFlags)

$(outDir)/%.v.glsl.o: $(shaderDir)/%.v.glsl
	bash $(shaderDir)/shader.cpp.sh "$*VertexShaderCode" "$(shaderDir)/$*.v.glsl" | $(cpp) -x c++ -c -o $@ -

$(outDir)/%.f.glsl.o: $(shaderDir)/%.f.glsl
	bash $(shaderDir)/shader.cpp.sh "$*FragmentShaderCode" "$(shaderDir)/$*.f.glsl" | $(cpp) -x c++ -c -o $@ -

.PHONY: all dir
