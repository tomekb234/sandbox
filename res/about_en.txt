Simple physics sandbox

Copyright (c) 2020
Tomasz Buczyński, Bartłomiej Wacławik

Licensed under the MIT license.

Used libraries:
- Bullet physics engine
- OpenGL
- JavaFX

Icons from icons8.com
